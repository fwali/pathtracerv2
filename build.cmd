@echo off

REM PROJECT VARIABLES
set EXE_NAME=PathTracer
set OUTPUT_DIR=..\bin\debug
set COMPILER_ARGS=/EHsc /W3 /Gm /MTd /I ..\src /Zi /O2
set SOURCE_FILES=..\src\main.cpp

REM DIRECTORY SETUP
IF NOT EXIST bin (
    mkdir bin
)
IF NOT EXIST bin\debug (
    mkdir bin\debug
)

set BUILD_CMD=cl /nologo %COMPILER_ARGS% %SOURCE_FILES% /link /out:%OUTPUT_DIR%\%EXE_NAME%.exe
set ESC=[
set GREEN=[92m
set YELLOW=%ESC%93m
set RED=%ESC%91m
set NC=[0m

REM PRE-BUILD
call pre_build.cmd

REM BUILD
cd build/
%BUILD_CMD% > build.output
set CL_EXIT_CODE=%ERRORLEVEL%

REM OUTPUT SUMMARY
echo --------------------- OUTPUT --------------------------------
echo %YELLOW%Warnings:
findstr "warning" build.output
echo %RED%Errors:
findstr "error" build.output
findstr "undefined" build.output
echo %NC%

echo Compiler exited with: %CL_EXIT_CODE%
IF %CL_EXIT_CODE% EQU 0 (
    echo %GREEN%Build succeeded @%time% %NC%
) ELSE (
    echo %RED%Build FAILED @%time%%NC%
)

REM POST-BUILD
cd ../
IF %CL_EXIT_CODE% EQU 0 (
    call post_build.cmd
)

EXIT /B %CL_EXIT_CODE%