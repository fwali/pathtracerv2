#pragma once

#include <vector>

#include "lib/Math.h"


vec2 ConcentricSampleDisk(const vec2& p );

vec3 UniformSampleHemisphere( const vec2& p );
inline vec3 CosineSampleHemisphere( const vec2& p )
{
    vec2 u = ConcentricSampleDisk( p );
    float z = sqrt( _MAX( 0.f, 1 - u.x*u.x - u.y*u.y ) );
    return vec3( u.x, u.y, z );
}

inline float UniformHemispherePdf()
{
    return INV_TAU;
}

vec3 UniformSampleSphere( const vec2& u );
inline float UniformSpherePdf( const vec2& u)
{
    return 0.25f * INV_PI;  // 1/(4*PI)
}

vec3 UniformSampleCone(const vec2 &u, float cosThetaMax);
inline float UniformConePdf(float cosThetaMax) {
    return 1.f / (2.f * PI * (1.f - cosThetaMax));
}

//
//  ISampler interface
//
struct ISampler
{
    const size_t    m_samplesPerPixel;
    size_t          m_1DArrayOffset;
    size_t          m_2DArrayOffset;

    vec2            m_currentPixel;
    size_t          m_currentPixelSampleIndex;  // number of samples generated for the current pixel

    std::vector< u32 > m_samples1DArraySizes {};
    std::vector< u32 > m_samples2DArraySizes {};
    std::vector< std::vector< float > > m_samplesArray1D    {};
    std::vector< std::vector< vec2 > >  m_samplesArray2D    {};

    explicit ISampler( size_t samplesPerPixel );
    virtual ~ISampler();

    virtual float Get1D() = 0;
    virtual vec2  Get2D() = 0;

    void Request1DArray( u32 n );
    void Request2DArray( u32 n );

    virtual int RoundCount( int n ) 
    {
        return n;
    }

    virtual ISampler* Clone( int seed ) = 0;

    const float* Get1DArray( int index );
    const vec2*  Get2DArray( int index );

    virtual bool StartNextSample();
    virtual void StartPixel( vec2 pixelPos );
};

struct PixelSampler : ISampler
{
    std::vector< std::vector< float > > m_samples1D {};
    std::vector< std::vector< vec2 > >  m_samples2D {};
    u32 m_current1DDimension = 0;
    u32 m_current2DDimension = 0;
    u32 m_numSampledDimensions = 0;
    RNG m_rng;

    PixelSampler( size_t samplesPerPixel, u32 nSampledDimensions, int seed = __rdtsc() );

    //  ISampler interface overrides
    //
	float Get1D() override;
    vec2  Get2D() override;
    bool  StartNextSample() override;
    virtual ISampler* Clone( int seed ) override;
};

struct StratifiedSampler : PixelSampler
{
    const size_t m_pixelSamplesX; // Number of horizontal samples to take w.r.t. camera's image plane
    const size_t m_pixelSamplesY; // Number of vertical samples to take w.r.t. camera's image plane
    const bool   m_jitterSamples;

    StratifiedSampler( size_t horizontalSamples, size_t verticalSamples, bool jitterSamples, int nSampledDims, int seed = __rdtsc()  );

    //  ISampler interface overrides
    //
    void StartPixel( vec2 pixelPos ) override;
    ISampler* Clone( int seed ) override;
};
