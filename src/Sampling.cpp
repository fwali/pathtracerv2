
#include "Sampling.h"

vec2 ConcentricSampleDisk(const vec2& p )
{
    vec2 newSample = {};

    vec2 uOffset = 2.f*p - vec2( 1.0f );
    if ( uOffset.x == 0.f && uOffset.y == 0.f ) return newSample;

    float theta, r;
    if ( _ABS( uOffset.x ) > _ABS( uOffset.y ) )
    {
        r = uOffset.x;
        theta = PI_OVER_4 * ( uOffset.y/uOffset.x );
    }
    else
    {
        r = uOffset.y;
        theta = PI_OVER_4 * ( uOffset.x/uOffset.y );
    }

    newSample = r * vec2( cos( theta ), sin( theta ) );

    return newSample;
}

vec3 UniformSampleHemisphere( const vec2& p )
{
    vec3 sample = {};
    float z = p.x;
    float r = sqrt( _MAX( 0.f, 1.0 - z*z ) );
    float phi = 2 * PI * p.y;
    sample = vec3( r * cos( phi ), r * sin( phi ), z );
    return sample;
}

vec3 UniformSampleSphere( const vec2& u )
{
    float z = 1.f - 2*u.y;
    float r = std::sqrt( _MAX( 0.f, 1.f - z*z ) );
    float phi = TAU*u.x;
    return vec3( r * std::cos( phi ), r*std::sin( phi ), z  );
}

vec3 UniformSampleCone(const vec2 &u, float cosThetaMax)
{
    float cosTheta = ((float)1 - u.x) + u.x * cosThetaMax;
    float sinTheta = std::sqrt((float)1 - cosTheta * cosTheta);
    float phi = u.y * 2 * PI;
    return vec3(std::cos(phi) * sinTheta, std::sin(phi) * sinTheta,
                    cosTheta);
}

//
//  ISampler Impl
//

ISampler::ISampler( size_t samplesPerPixel )
:
    m_samplesPerPixel( samplesPerPixel )
{}

ISampler::~ISampler()
{}

void
ISampler::StartPixel( vec2 pixelPos )
{
    m_currentPixel  = pixelPos;
    m_currentPixelSampleIndex = 0u;
    m_1DArrayOffset = m_2DArrayOffset = 0u;
}

bool
ISampler::StartNextSample()
{
    m_1DArrayOffset = m_2DArrayOffset = 0u;
    ++m_currentPixelSampleIndex;

    return m_currentPixelSampleIndex < m_samplesPerPixel;
}

void
ISampler::Request1DArray( u32 n )
{
    m_samples1DArraySizes.push_back( n );
    m_samplesArray1D.push_back( std::vector< float >( n * m_samplesPerPixel  ) );
}

void
ISampler::Request2DArray( u32 n )
{
    m_samples2DArraySizes.push_back( n );
    m_samplesArray2D.push_back( std::vector< vec2 >( n * m_samplesPerPixel ) );
}

const float*
ISampler::Get1DArray( int index )
{
    if ( m_1DArrayOffset == m_samples1DArraySizes.size() ) { return nullptr; }
    
    return &m_samplesArray1D[ m_1DArrayOffset++ ][ m_currentPixelSampleIndex*index ];
}

const vec2*
ISampler::Get2DArray( int index )
{
    if ( m_2DArrayOffset == m_samples2DArraySizes.size() ) { return nullptr; }

    return &m_samplesArray2D[ m_2DArrayOffset++ ][ m_currentPixelSampleIndex*index ];
}

//
//  PixelSampler impl
//

PixelSampler::PixelSampler( size_t samplesPerPixel, u32 nSampledDimensions, int seed )
:
    ISampler( samplesPerPixel ),
    m_samples1D(),
    m_samples2D(),
    m_numSampledDimensions( nSampledDimensions ),
    m_rng( seed )
{
    for ( int i = 0u; i < nSampledDimensions; ++i )
    {
        m_samples1D.push_back( std::vector< float >( samplesPerPixel ) );
        m_samples2D.push_back( std::vector< vec2 >( samplesPerPixel ) );
    }
}

bool
PixelSampler::StartNextSample()
{
    m_current1DDimension = m_current2DDimension = 0u;

    return ISampler::StartNextSample();
}

float
PixelSampler::Get1D()
{
    if ( m_current1DDimension < m_samples1D.size() )
    {
        return m_samples1D[ m_current1DDimension++ ][ m_currentPixelSampleIndex ];
    }
    else
    {
        return m_rng.rand();
    }
}

vec2
PixelSampler::Get2D()
{
    if ( m_current2DDimension < m_samples2D.size() )
    {
        return m_samples2D[ m_current2DDimension++ ][ m_currentPixelSampleIndex ];
    }
    else
    {
        return { m_rng.rand(), m_rng.rand() };
    }
}

ISampler*
PixelSampler::Clone( int seed )
{
    return new PixelSampler( m_samplesPerPixel, m_numSampledDimensions, seed );
}

//  Sampling utility functions
//

void 
StratifiedSampling1D( float* p_sample, int nSamples, RNG& rng, bool jitter )
{
    float invNSamples = 1.f/float(nSamples);
    for ( int i = 0; i < nSamples; ++i )
    {
        float delta = jitter ? rng.rand() : 0.5f;
        p_sample[ i ] = _MIN( ( i + delta ) * invNSamples, ONE_MINUS_EPSILON );
    }
}

void
StratifiedSampling2D(  vec2* p_samples, int nSamplesX, int nSamplesY, RNG& rng, bool jitter )
{
    float invSamplesX = 1.f/float(nSamplesX);
    float invSamplesY = 1.f/float(nSamplesY);

    for ( int y = 0; y < nSamplesY; ++y )
    {
        for ( int x = 0; x < nSamplesX; ++x )
        {
            float jx = jitter ? rng.rand() : 0.5f;
            float jy = jitter ? rng.rand() : 0.5f;

            p_samples->x = _MIN( ( float(x) + jx ) * invSamplesX, ONE_MINUS_EPSILON );
            p_samples->y = _MIN( ( float(y) + jy ) * invSamplesY, ONE_MINUS_EPSILON );

            ++p_samples;
        }
    }
}


template <typename T>
void Shuffle( T* p_samples, int count , int nDimensions, RNG& rng )
{
    for ( int i = 0; i < count; ++i )
    {
        int other = _MIN( i + rng.rand()*( count - i ), count - 1 );
        for ( int j = 0; j < nDimensions; ++j )
        {
            SWAP( p_samples[ nDimensions*i + j ], p_samples[ nDimensions*other + j ] );
        }
    }
}

void
LatinHypercube( float* p_samples, int nSamples, int dims, RNG& rng )
{
    float invNSamples = 1.f / float(nSamples);
    for ( int i = 0; i < nSamples; ++i )
    {
        for ( int j = 0; j < dims; ++j )
        {
            float sj = ( i + rng.rand() ) * invNSamples;
            p_samples[ dims*i + j ] = _MIN( sj, ONE_MINUS_EPSILON );
        }
    }

    for ( int i = 0; i < dims; ++i )
    {
        for ( int j = 0; j < nSamples; ++j )
        {
            int other =  j + rng.rand()*( nSamples - j );
            SWAP( p_samples[ dims*j + i], p_samples[ dims*other + i ] );
        }
    }
}

//
//  StratifiedSampler Impl
//

StratifiedSampler::StratifiedSampler(
    size_t  horizontalSamples,
    size_t  verticalSamples,
    bool    jitterSamples,
    int     nSampledDimensions,
    int     seed )
:
    PixelSampler( horizontalSamples * verticalSamples, nSampledDimensions, seed ),
    m_pixelSamplesX( horizontalSamples ),
    m_pixelSamplesY( verticalSamples ),
    m_jitterSamples( jitterSamples )
{}

void
StratifiedSampler::StartPixel( vec2 pixelPos )
{
    size_t numPixelSamples  = m_pixelSamplesX * m_pixelSamplesY;
    bool   jitter           = m_jitterSamples;

    // Generate single stratified samples for this pixel
    //
    for ( size_t i = 0u; i < m_samples1D.size(); ++i )
    {
        StratifiedSampling1D( m_samples1D[ i ].data(), m_pixelSamplesX * m_pixelSamplesY, m_rng, jitter );
        Shuffle( m_samples1D[ i ].data(), m_pixelSamplesX * m_pixelSamplesY, 1 /* dim */, m_rng );
    }


    for ( size_t i = 0u; i < m_samples2D.size(); ++i )
    {
        StratifiedSampling2D( m_samples2D[ i ].data(), m_pixelSamplesX, m_pixelSamplesY, m_rng, jitter );
        Shuffle( m_samples2D[ i ].data(), m_pixelSamplesX*m_pixelSamplesY, 1 /* dim */, m_rng );
    }

// FW_TODO: Need to fix this, this is screwing up the stratified sampler
//
#if 1
    //  Generate arrays of stratified samples for pixel
    //
    for ( size_t i = 0u; i < m_samples1DArraySizes.size(); ++i )
    {
        for ( size_t j = 0u; j < m_samplesPerPixel; ++j )
        {
            u32 count = m_samples1DArraySizes[i];
            StratifiedSampling1D( &m_samplesArray1D[ i ][ j*count ], numPixelSamples, m_rng, jitter );
            Shuffle( &m_samplesArray1D[ i ][ j*count ], count, 1 /* dim */, m_rng );
        }
    }
    
    for ( size_t i = 0u; i < m_samples2DArraySizes.size(); ++i )
    {
        for ( size_t j = 0u; j < m_samplesPerPixel; ++j )
        {
            int count = m_samples2DArraySizes[ i ];
            LatinHypercube( &m_samplesArray2D[ i ][ j*count ].x, count, 2, m_rng );
        }
    }
#endif
    PixelSampler::StartPixel( pixelPos );
}

ISampler*
StratifiedSampler::Clone( int seed )
{
    return new StratifiedSampler( m_pixelSamplesX, m_pixelSamplesX, m_jitterSamples, m_numSampledDimensions, seed );
}
