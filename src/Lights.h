#pragma once

#include "lib/Math.h"

#include "Interactions.h"
#include "Shape.h"

struct Scene;
struct Sampler;

//  Used for casting shadow rays toward light sources (Next Event Estimation)
//
struct VisibilityTester
{
    Interaction m_p0;
    Interaction m_p1;

    VisibilityTester() = default;
    VisibilityTester( const Interaction& p0, const Interaction& p1 )
    :
        m_p0( p0 ),
        m_p1( p1 )
    {}

    bool Unoccluded( const Scene& scene ) const;
    // F_TODO:: Update to handle particpating media
    // vec3 Transmittance( const Scene& scene, Sampler& sampler ) const;
};

enum LightFlags : u32
{
    kLightFlags_DeltaPos = BIT(1),
    kLightFlags_DeltaDir = BIT(2),
    kLightFlags_Area     = BIT(3),
    kLightFlags_Infinite = BIT(4)
};

struct ILight
{
    const LightFlags    m_flags;
    const u32           m_numSamples;

    // F_TODO:: Need update to handle participating media
    //

    ILight( LightFlags flags, u32 numSamples = 1 )
    :
        m_flags( flags ),
        m_numSamples( m_numSamples )
    {}

    inline bool IsDeltaLight() const
    {
        return ( m_flags & ( kLightFlags_DeltaPos | kLightFlags_DeltaDir ) ) != 0u;
    }

    virtual void Preprocess( const Scene* p_scene ) {}

    //  Sample light from this source arriving at a location defined by the interaction object
    //
    virtual vec3 Sample_Li(
        const Interaction& ref,
        vec2 u,
        vec3& o_wi,
        float& o_pdf,
        VisibilityTester& o_visTester ) const = 0;

    virtual float PDF_Li( const Interaction& ref, vec3 wi ) const = 0;

    virtual vec3 Le( const Ray& ray ) const = 0;

    /*  // Sampling the Light source's emission profile
    virtual vec3 Sample_Le( vec2 u1, vec2 u2, float time,
                            Ray& o_ray, vec3& o_normalLight,
                            float& o_pdfPos, float& o_pdfDir ) const = 0;

    virtual void PDF_Le( const Ray& ray, const vec3& normalLight, float& o_pdfPos, float o_pdfDir ) const = 0;
    */

    virtual ~ILight() {}
};

struct PointLight : ILight
{
    vec3    m_pos;
    vec3    m_lightIntensity;

    PointLight( vec3 pos, vec3 lightIntensity )
    :
        ILight( kLightFlags_DeltaPos ),
        m_pos( pos ),
        m_lightIntensity( lightIntensity )
    {}

    vec3 Le( const Ray& ray ) const override;

    //  Sample light from this source arriving at a location defined by the interaction object
    //
    vec3 Sample_Li(
        const Interaction& ref,
        vec2 u,
        vec3& o_wi,
        float& o_pdf,
        VisibilityTester& o_visTester ) const override;

    float PDF_Li( const Interaction& ref, vec3 wi ) const override;
};

struct IAreaLight : ILight
{
    IAreaLight( u32 numSamples )
    :
        ILight( kLightFlags_Area, numSamples )
    {}

    //  Sample the emission from a point on this area light source given by intx
    //  Emitted toward wo
    //
    virtual vec3 L( const Interaction& intx, const vec3& wo ) const = 0;
};

struct DiffuseAreaLight : IAreaLight
{
    vec3        m_emission;
    float       m_area;
    IShape*     m_shapePtr;

    DiffuseAreaLight( vec3 emission, int nSamples, IShape* p_shape )
    :
        IAreaLight( nSamples ),
        m_emission( emission ),
        m_shapePtr( p_shape )
    {
        if ( m_shapePtr )
        {
            m_area = p_shape->Area();
        }
    }

    vec3 Le( const Ray& ray ) const override;

    //  Sample light from this source arriving at a location defined by the interaction object
    //
    vec3 Sample_Li(
        const Interaction& ref,
        vec2 u,
        vec3& o_wi,
        float& o_pdf,
        VisibilityTester& o_visTester ) const override;

    float PDF_Li( const Interaction& ref, vec3 wi ) const override;

    //  Sample the emission from a point on this area light source given by intx
    //  Emitted toward wo
    //
    vec3 L( const Interaction& intx, const vec3& wo ) const override
    {
        return ( dot( intx.m_normal, wo ) > 0.f ) ? m_emission : vec3( 0.f );
    }

};


#ifdef FW_LIGHTS_IMPL

#include "Scene.h"

bool VisibilityTester::Unoccluded( const Scene& scene ) const
{
    return !scene.IntersectsP( m_p0.CreateRayTo( m_p1.m_pos ) );
}


vec3 PointLight::Le( const Ray& ray ) const
{
    return {};
}

vec3 PointLight::Sample_Li(
        const Interaction& ref,
        vec2 u,
        vec3& o_wi,
        float& o_pdf,
        VisibilityTester& o_visTester ) const
{
    vec3 radiance {};

    vec3 lightVec = m_pos - ref.m_pos;
    Interaction lightInteraction( m_pos, {}, 0.f );
    o_wi = normalize( lightVec );
    o_pdf = 1.0f;
    o_visTester = VisibilityTester( ref, lightInteraction );

    float invDistSquared = 1.f / dot( lightVec, lightVec );

    return invDistSquared * m_lightIntensity;
}

float PointLight::PDF_Li( const Interaction& ref, vec3 wi ) const
{
    return 1.0f;
}

//
//  Diffuse Area light
//

vec3
DiffuseAreaLight::Le( const Ray& ray ) const
{
    return {};
}

//  Sample light from this source arriving at a location defined by the interaction object
//
vec3
DiffuseAreaLight::Sample_Li(
    const Interaction& ref,
    vec2 u,
    vec3& o_wi,
    float& o_pdf,
    VisibilityTester& o_visTester ) const
{
    Interaction shapeIntx = m_shapePtr->Sample( ref, u );
    // shapeIntx.mediumInterface = mediumInterface /* from light? */ F_TODO::
    o_wi    = normalize( shapeIntx.m_pos - ref.m_pos );
    o_pdf   = m_shapePtr->Pdf( ref, o_wi );

    o_visTester = VisibilityTester( ref, shapeIntx );

    return this->L( shapeIntx, -1.f*o_wi );
}

float
DiffuseAreaLight::PDF_Li( const Interaction& ref, vec3 wi ) const
{
    return m_shapePtr->Pdf( ref, wi );
}


#endif
