#pragma once

#include "lib/Math.h"
#include "Interactions.h"

struct IShape
{
    IShape() {}
    virtual ~IShape();

    virtual AABB    ComputeBBox() const = 0;
    virtual bool    Intersects( const Ray& ray, IntersectionResult&  ) const = 0;
    virtual void    GetShadingInfo( SurfaceInteraction&, const IntersectionResult&  ) const = 0;
    virtual float   Area() const = 0;

    //
    //  Required for sampling shapes as area light sources
    //

    virtual float   Pdf( const Interaction& ) const
    {
        return 1.f / Area();
    }
    virtual float   Pdf( const Interaction& intx, const vec3& wi ) const;

    virtual Interaction Sample( const vec2& u ) const = 0;

    //  Sample (this) shape based on a reference point provided by ref
    //
    virtual Interaction Sample( const Interaction& ref, const vec2& u ) const
    {
        return Sample( u );
    }
};

struct Shape_Sphere : public IShape
{
    Sphere m_sphere;
    vec2   m_uvScale;

    Shape_Sphere( vec3 origin, float radius, vec2 uvScale = vec2(1.0f) ):
        IShape(),
        m_sphere( origin, radius ),
        m_uvScale( uvScale )
    {}

    AABB ComputeBBox() const override
    {
        vec3 radius = vec3( m_sphere.m_radius );
        AABB aabb( m_sphere.m_centre - radius, m_sphere.m_centre + radius );

        return aabb;
    }

    void GetShadingInfo( SurfaceInteraction& surfInt, const IntersectionResult&   ) const override;

    bool Intersects( const Ray& ray, IntersectionResult& surfaceInteraction ) const override
    {
        return Intersection_Ray_Spheres( surfaceInteraction, &ray, 1, &m_sphere );
    }

    //  SA of Sphere = 4 * PI * R^2
    //
    float Area() const override
    {
        float r2 = m_sphere.m_radius * m_sphere.m_radius;
        return 4.f * PI * r2;
    }

    float Pdf( const Interaction& intx, const vec3& wi ) const override;

    Interaction Sample( const vec2& u ) const override;

    Interaction Sample( const Interaction& ref, const vec2& u ) const override;
};

struct TriangleMeshData;
struct TriangleShape : IShape
{
    TriangleMeshData*   m_triMeshPtr    {};
    const u32*          m_indexPtr      {};

    TriangleShape( TriangleMeshData* p_triMesh, const u32* p_index )
    :
        IShape(),
        m_triMeshPtr( p_triMesh ),
        m_indexPtr( p_index )
    {}

    AABB ComputeBBox() const override;

    void GetShadingInfo( SurfaceInteraction& , const IntersectionResult&   ) const override;

    bool Intersects( const Ray& ray, IntersectionResult& surfaceInteraction ) const override;

    float Area() const override;

    Interaction Sample( const vec2& u ) const override;
};



struct TriangleMeshData
{
    const size_t m_numTris;
    const size_t m_numVerts;

    //  These pointers don't own any data
    //
    u32*  m_pIndices    = nullptr;
    vec3* m_pVerts      = nullptr;
    vec3* m_pNormals    = nullptr;
    vec2* m_pUVs        = nullptr;
    vec3* m_pTangents   = nullptr;
    AABB  m_bbox        {};

    TriangleMeshData(   size_t numTris, size_t numVerts,
                        u32* pIndices, vec3* pVerts,
                        vec2* pUVs, vec3* pNormals, vec3* pTangents,
                        const Mat4x4& transform = Mat4x4_Identity() )
    :
        m_numTris( numTris ),
        m_numVerts( numVerts ),
        m_pIndices( pIndices ),
        m_pVerts( pVerts ),
        m_pNormals( pNormals ),
        m_pUVs( pUVs ),
        m_pTangents( pTangents )
    {
        //  Apply transform to P, and N
        //
        if ( m_pVerts )
        {
            for ( size_t i = 0u; i < numVerts; ++i )
            {
                pVerts[ i ] = Mat4x4_TransformPoint( transform, pVerts[ i ] );

                AABB_Expand( m_bbox, pVerts[ i ] );

                if ( pNormals )
                {
                    pNormals[ i ] = normalize( Mat4x4_TransformDirection( transform, pNormals[ i ] ) );
                }

                if ( pTangents )
                {
                    pTangents[ i ] = normalize( Mat4x4_TransformDirection( transform, pTangents[ i ] ) );
                }
            }
        }
    }

};
