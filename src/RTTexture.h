#pragma once

#include "lib/Math.h"

struct IRtTexture
{
    virtual ~IRtTexture() {}
    virtual vec3 Sample( vec2 uv, vec3 p ) const = 0;
};

struct FlatColour : public IRtTexture
{
    vec3 m_value;

    FlatColour( vec3 colour ):
        IRtTexture(),
        m_value( colour )
    {}

    ~FlatColour() override {}

    vec3 Sample( vec2 uv, vec3 p ) const override
    {
        return m_value;
    }
};

struct CheckerTexture : IRtTexture
{
    vec3 m_odd = {};
    vec3 m_even = {};

	CheckerTexture() {}
	CheckerTexture( vec3 odd, vec3 even ) :
        m_odd( odd ), m_even( even )
    {}

	// Inherited via Texture
    vec3 Sample( vec2 uv, vec3 p ) const override
    {
		float sines = sin( p.x)*sin(p.y)*sin( p.z);
		if (sines < 0)
			return m_odd;
		else
			return m_even;
	}
};


struct WaveTexture : IRtTexture
{
    vec3 m_scale;

    explicit WaveTexture( vec3 scale )
    :
        IRtTexture(),
        m_scale( scale )
    {}

	// Inherited via Texture
    vec3 Sample( vec2, vec3 p ) const override
    {
        #if 0
		float cosines = 0.5f*(cos( m_scale.x*p.x )*cos( m_scale.z*p.z ) + 1.f );

        return vec3( cosines*cosines );
        #else

        // Project P onto the X-Z plane
        //
        vec3 projP = { p.x, 0.f, p.z };
        float radius = length( projP );
        float alpha = _MAX( 1.f, 0.5f*sin( radius*2.0f ) + 0.501f );
        // float alpha = _MAX( 0.f, 0.3f*sin( radius ) );
        return vec3( alpha );

        #endif
	}
};

struct ImageTexture : public IRtTexture
{
    byte*   m_imgData   = nullptr;
    i32     m_imgWidth  = -1;
    i32     m_imgHeight = -1;
    i32     m_numChannels = -1;

    ImageTexture( byte* pData, int width, int height, int numChannels ) :
        m_imgData( pData), m_imgWidth( width ), m_imgHeight( height ), m_numChannels( numChannels )
    {
        #ifdef DEBUG_BUILD
        ASSERT_TRUE( m_imgData != nullptr, "Creating ImageTexture with null image data (pData)" )
        // ASSERT_TRUE( )
        #endif
    }

    ~ImageTexture() override {}
    
    vec3 Sample( vec2 uv, vec3 ) const override
    {
        vec3 colour;

        if ( m_imgData )
        {
            byte* pData     = m_imgData;
            int nx          = m_imgWidth;
            int ny          = m_imgHeight;

            int i = uv.u * nx;
            int j = (1.0f - uv.v) * ny - 0.001;
            CLAMP_VAR( i, 0, nx - 1);
            CLAMP_VAR( j, 0, ny - 1 );


            float r = int(pData[3 * i + 3 * nx*j]) / 255.0;
            float g = int(pData[3 * i + 3 * nx*j + 1]) / 255.0;
            float b = int(pData[3 * i + 3 * nx*j + 2]) / 255.0;

            r = pow( r, 1.0/2.2 );
            g = pow( g, 1.0/2.2 );
            b = pow( b, 1.0/2.2 );

            colour = { r, g, b };
        }
        else
        {
            colour = { 0.9f, 0.0f, 0.9f };
        }

		return colour;
    }
};