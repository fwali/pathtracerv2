#pragma once

#include "lib/Math.h"

struct IPrimitive;
struct Material;
struct Bsdf;

struct Interaction
{
    vec3    m_pos;
    vec3    m_normal;
    vec3    m_posError;
    float   m_time;
    vec3    m_wo;   // Outgoing-ray direction

    Ray    CreateRay( vec3 dir ) const;
    Ray    CreateRayTo( vec3 endPoint ) const;

    virtual bool isSurfaceInteraction() const
    {
        return false;
    }

    Interaction() {}
    Interaction( vec3 pos, vec3 normal, float time )
    :
        m_pos( pos ),
        m_normal( normal ),
        m_posError(),
        m_time( time ),
        m_wo()
    {}
};

struct SurfaceInteraction : public Interaction
{
    vec2                m_uv;
    vec3                m_dpdu, m_dpdv; // Partial derivatives of m_pos with respect to uv (dp/du and dp/dv)
    vec3                m_dndu, m_dndv; // Partial derivatives of m_normal with respect to uv (dn/du, dn/dv)

    struct Shading
    {
        vec3    n;
        vec3    dpdu, dpdv;
        vec3    dndu, dndv;
    };
    Shading             m_shading;
    

    Material*           m_materialPtr;  // F_TODO:: Get rid of this member
    const IPrimitive*   m_primPtr;
    Bsdf*               m_bsdf = nullptr;

    SurfaceInteraction() {}

    SurfaceInteraction( const vec3& pos, const vec2& uv, const vec3& n,
                        const vec3& dpdu, const vec3& dpdv,
                        const vec3& dndu, const vec3& dndv, 
                        float time )
    :
        Interaction( pos, n, time ),
        m_uv( uv ),
        m_shading { n, dpdu, dpdv, dndu, dndv }
    {}

    bool isSurfaceInteraction() const override
    {
        return true;
    }

    //  Equivalent to PBRT's Le method
    //
    vec3 GetAreaLightEmission( const vec3& wo ) const;

    void ComputeScatteringFunctions(    IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes );
};

vec3 OffsetRayOrigin(
    const vec3 &p, const vec3 &pError,
    const vec3 &n, const vec3 &w);

#ifdef FW_INTERACTIONS_IMPL

#include "Material.h"
#include "Lights.h"

vec3
OffsetRayOrigin(const vec3 &p, const vec3 &pError,
                               const vec3 &n, const vec3 &w) {
    float d = dot(abs(n), pError);
    vec3 offset = d * n;
    if (dot(w, n) < 0)
    {
        offset = -1.f*offset;
    }
    vec3 po = p + offset;
    // <<Round offset point po away from p>> 
       for (int i = 0; i < 3; ++i) {
           if (offset.v[i] > 0)      po.v[i] = NextFloatUp(po.v[i]);
           else if (offset.v[i] < 0) po.v[i] = NextFloatDown(po.v[i]);
       }

    return po;
}

Ray Interaction::CreateRay( vec3 dir ) const
{
    Ray newRay {};

    newRay.m_origin = m_pos + 10.f*EPSILON*m_normal;
    // newRay.m_origin = OffsetRayOrigin( m_pos, m_posError, m_normal, dir );
    newRay.m_dir = normalize( dir );
    newRay.m_tMax = FLOAT_MAX;

    return newRay;
}

Ray Interaction::CreateRayTo( vec3 endPoint ) const
{
    Ray newRay = Ray::CreateFiniteRay( m_pos + EPSILON*m_normal, endPoint );

    // F_TODO:: Handle floating point error more robustly than this,...
    newRay.m_tMax -= 0.0001f;

    return newRay; 
}

void SurfaceInteraction::ComputeScatteringFunctions(
    IMemAlloc* memAllocator,
    bool transportFromEye,
    bool allowMultipleLobes )
{
    if ( m_primPtr && m_materialPtr )
    {
        m_materialPtr->ComputeScatteringFunctions( this, memAllocator, transportFromEye, allowMultipleLobes );
    }    
}

vec3 SurfaceInteraction::GetAreaLightEmission( const vec3& wo ) const
{
    const IAreaLight* p_light = m_primPtr->GetAreaLight();
    return ( p_light ) ? p_light->L( *this, wo ) : vec3( 0.f );
}

#endif