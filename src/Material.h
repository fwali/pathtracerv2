#pragma once

#include "lib/Math.h"

#include "RTTexture.h"
#include "Interactions.h"
#include "Bxdf.h"

struct Primitive;
struct IMemAlloc;
struct Material;


struct Material
{
    Material() = default;

    virtual void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                                IMemAlloc* memAllocator,
                                                bool transportFromEye,
                                                bool allowMultipleLobes ) const = 0;
};

struct MatteMaterial : Material
{
    IRtTexture* m_albedo;

    MatteMaterial( IRtTexture* albedo, IRtTexture* roughness=nullptr)
    :
        Material(),
        m_albedo( albedo )
    {}

    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;
};

struct ReflectiveMaterial : Material
{
    IRtTexture* m_albedo;
    float       m_IOR;  // index of refraction

    ReflectiveMaterial( IRtTexture* albedo, float IOR )
    :
        Material(),
        m_albedo( albedo ),
        m_IOR( IOR )
    {}

    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;
};


struct RefractiveMaterial : Material
{
    float m_IOR;

    RefractiveMaterial( float IOR )
    :
        Material(),
        m_IOR( IOR )
    {}


    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;   
};

struct FresnelSpecMaterial : Material
{
    IRtTexture* m_albedo;
    IRtTexture* m_transmissive;
    float       m_etaI;
    float       m_etaT;

    FresnelSpecMaterial( IRtTexture* albedo, IRtTexture* transmissive, float etaI, float etaT )
    :
        Material(),
        m_albedo( albedo ),
        m_transmissive( transmissive ),
        m_etaI( etaI ),
        m_etaT( etaT )
    {}

    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;
};

struct MicrofacetMaterial : Material
{
    IRtTexture* m_albedo;
    IRtTexture* m_specColour;
    IRtTexture* m_roughness;

    MicrofacetMaterial( IRtTexture* albedo, IRtTexture* specColour, IRtTexture* roughness )
    :
        Material(),
        m_albedo( albedo ),
        m_specColour( specColour ),
        m_roughness( roughness )
    {}


    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;   
};


struct PlasticMaterial : Material
{
    IRtTexture* m_albedo;
    IRtTexture* m_spec;
    IRtTexture* m_roughness;

    PlasticMaterial( IRtTexture* albedo, IRtTexture* specColour, IRtTexture* roughness )
    :
        Material(),
        m_albedo( albedo ),
        m_spec( specColour ),
        m_roughness( roughness )
    {}


    void ComputeScatteringFunctions(    SurfaceInteraction* si,
                                        IMemAlloc* memAllocator,
                                        bool transportFromEye,
                                        bool allowMultipleLobes ) const override;   

};

#ifdef FW_MATERIAL_IMPL

#include "lib/Memory.h"

#define PUSH( Type )   ALLOC_NEW( *p_memAlloc, Type )


void
MatteMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    si->m_bsdf = ALLOC_NEW( *p_memAlloc, Bsdf ) ( si->m_normal,  si->m_shading.n, si->m_shading.dpdu );

    vec3 reflectance = m_albedo->Sample( si->m_uv, si->m_pos );

    si->m_bsdf->Add( ALLOC_NEW( *p_memAlloc, LambertianReflection ) ( reflectance ) );
}

void
ReflectiveMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    si->m_bsdf = PUSH(  Bsdf ) ( si->m_normal,  si->m_shading.n, si->m_shading.dpdu );

    vec3 albedo = m_albedo->Sample( si->m_uv, si->m_pos );
    Fresnel* p_fresnel = PUSH( FresnelDielectric ) ( 1.0f /* incident IOR */, m_IOR );
    // Fresnel* p_fresnel = PUSH( FresnelNoOp );

    si->m_bsdf->Add( PUSH( SpecularReflection ) ( albedo, p_fresnel ) );

    // si->m_bsdf->Add( PUSH( SpecularReflection ) ( reflectance, 1.0f, m_IOR ) );

}

void
RefractiveMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    si->m_bsdf = ALLOC_NEW( *p_memAlloc, Bsdf ) ( si->m_normal,  si->m_shading.n, si->m_shading.dpdu );

    vec3 transmittance = vec3( 1.f );

    // si->m_bsdf->Add( ALLOC_NEW( *p_memAlloc, SpecularTransmission ) ( transmittance, 1.0f, m_IOR, ALLOC_NEW( FresnelNoOp ) () ) );
}

void
FresnelSpecMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    auto* bsdf = ALLOC_NEW( *p_memAlloc, Bsdf ) ( si->m_normal, si->m_shading.n, si->m_shading.dpdu );
    si->m_bsdf = bsdf;

    vec3 albedo = m_albedo->Sample( si->m_uv, si->m_pos );
    vec3 transmissive = m_transmissive->Sample( si->m_uv, si->m_pos );

    bsdf->Add( PUSH( FresnelSpecular ) ( albedo, transmissive, m_etaI, m_etaT, nullptr /* not necessary?*/ ) );
}


void
MicrofacetMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    si->m_bsdf = PUSH( Bsdf ) ( si->m_normal, si->m_shading.n, si->m_shading.dpdu );

    vec3 albedo = m_albedo->Sample( si->m_uv, si->m_pos );
    vec3 roughness = m_roughness->Sample( si->m_uv, si->m_pos );
    vec3 specColour = m_specColour->Sample( si->m_uv, si->m_pos );

    float alpha = Beckmann_RoughnessToAlpha( roughness.x );

    si->m_bsdf->Add( PUSH( LambertianReflection ) ( albedo ) );

    MicrofacetDistribution* p_dist = PUSH( BeckmannDistribution ) ( alpha, true );
    // si->m_bsdf->Add( PUSH( MicrofacetReflection ) ( albedo, specColour, p_dist, nullptr ) );
    si->m_bsdf->Add( PUSH( MicrofacetReflection ) ( albedo, /*specColour,*/ p_dist, nullptr ) );
}

void 
PlasticMaterial::ComputeScatteringFunctions(
    SurfaceInteraction* si,
    IMemAlloc* p_memAlloc,
    bool transportFromEye,
    bool allowMultipleLobes ) const
{
    si->m_bsdf = PUSH( Bsdf ) ( si->m_normal, si->m_shading.n, si->m_shading.dpdu );

    vec3 albedo = m_albedo->Sample( si->m_uv, si->m_pos );
    vec3 roughness = m_roughness->Sample( si->m_uv, si->m_pos );
    vec3 spec = m_spec->Sample( si->m_uv, si->m_pos );

#if 0
    if ( !VEC3_IS_ZERO( albedo ) )
    {
        si->m_bsdf->Add( PUSH( LambertianReflection ) ( albedo ) );
    }

    if ( !VEC3_IS_ZERO( spec ) )
    {
        float alpha = Beckmann_RoughnessToAlpha( roughness.x );

        auto p_microfacetDist = PUSH( BeckmannDistribution ) ( alpha, true );
        si->m_bsdf->Add( PUSH( MicrofacetReflection ) ( spec, p_microfacetDist, nullptr ) );

        // si->m_bsdf->Add()
    }
#else

    float alpha = Beckmann_RoughnessToAlpha( roughness.x );
    auto p_microfacetDist = PUSH( BeckmannDistribution ) ( alpha, true );

    si->m_bsdf->Add( PUSH( FresnelBlend ) ( albedo, spec, p_microfacetDist ) );

#endif
}

#endif