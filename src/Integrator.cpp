#include "Integrator.h"
#include "Scene.h"

#include <omp.h>
#include <vector>

//
//  SAMPLER INTEGRATOR
//

void SamplerIntegrator::Init( const Scene* p_scene, IMemAlloc* p_memArena )
{
    int ompNumThreads = omp_get_max_threads();
    m_perThreadMemArena.resize( ompNumThreads );

    //  Allocate 5 MB per thread arena
    //
    const size_t perThreadArenaSizeBytes = MB(5);
    for ( StackAlloc& arena : m_perThreadMemArena )
    {
        arena = StackAlloc( ALLOC_BYTES( (*p_memArena), perThreadArenaSizeBytes ), perThreadArenaSizeBytes );
    }


    //
    //
    m_perThreadSampler.resize( ompNumThreads );
    for ( int i = 0; i < ompNumThreads; ++i )
    {
        m_perThreadSampler[ i ] = m_sampler->Clone(  ((__rdtsc() << i) + i) & 0xF0F0FAFC );
    }

    std::cout <<"we initiated these samplers yo\n";
}

void SamplerIntegrator::Render( const Scene* p_scene, IMemAlloc* p_memArena )
{
    size_t imageWidth = m_renderImage->m_width;
    size_t imageHeight = m_renderImage->m_height;
    float* pImageBuffer = m_renderImage->m_buffer;

    Camera renderCam = m_camera;

    vec2 cameraDim = { float( imageWidth ), float( imageHeight ) };

    #pragma omp parallel for
    for ( size_t j = 0u; j < imageHeight; ++j )
    {
        for ( size_t i = 0u; i < imageWidth; ++i )
        {
            int threadID = omp_get_thread_num();
            ISampler* p_sampler = m_perThreadSampler[ threadID ];
            p_sampler->StartPixel( vec2( float(i), float(j) ) );

            vec3 pixelColour {};

            int nSamples = 0;
            do
            {
                // Jitter pixel sammples randomly for anti-aliasing
                //
                vec2 pixelPos = vec2( float(i), float(j) );
                pixelPos += p_sampler->Get2D();

                Ray pixelRay;
                CastRayFromCamera( &renderCam, pixelPos, cameraDim, pixelRay );

                pixelColour += this->Li( pixelRay, p_scene, p_sampler, & ( m_perThreadMemArena[ threadID ] ) );

                m_perThreadMemArena[ threadID ].Reset();
                ++nSamples;
            }
            while ( p_sampler->StartNextSample() );

            pixelColour *= 1.f/float(nSamples);

            float ir = pixelColour.r;
            float ig = pixelColour.g;
            float ib = pixelColour.b;

            size_t baseIndex = ( ( j*imageWidth ) + i ) * 3U;
            pImageBuffer[ baseIndex ]       += ir;
            pImageBuffer[ baseIndex + 1U ]  += ig;
            pImageBuffer[ baseIndex + 2U ]  += ib; 
        }
    }
}

vec3 SamplerIntegrator::SpecularReflect(
    const Ray& ray,
    const SurfaceInteraction& surfInt,
    const Scene* p_scene,
    ISampler* p_sampler,
    IMemAlloc* p_memArena,
    int depth ) const
{
    vec3 wo = surfInt.m_wo;
    vec3 wi = {};
    float pdf = 0.f;
    BxdfType type = BxdfType( kBxdfType_Specular | kBxdfType_Reflection );

    vec3 f = surfInt.m_bsdf->Sample_F( wo, wi, p_sampler->Get2D(), &pdf, type );

    const vec3& n = surfInt.m_shading.n;
    // F_TODO:: CHECK PBRT
    // float nDotWi = CLAMP( dot( n, wi ), 0.0f, 1.0f );
    float nDotWi = absDot( n, wi );
    if ( pdf > 0.f && nDotWi != 0.f )
    {
        Ray newRay = surfInt.CreateRay( wi );

        return f * Li( newRay, p_scene, p_sampler, p_memArena, depth + 1 ) * nDotWi / pdf;
    }
    else
    {
        return {};
    }
}
    
vec3 SamplerIntegrator::SpecularTransmit(   
    const Ray& ray,
    const SurfaceInteraction& surfInt,
    const Scene* p_scene,
    ISampler* p_sampler,
    IMemAlloc* p_memArena,
    int depth ) const
{
    vec3 wo = surfInt.m_wo;
    vec3 wi = {};
    float pdf = 0.f;
    BxdfType type = BxdfType( kBxdfType_Specular | kBxdfType_Transmission );

    vec3 f = surfInt.m_bsdf->Sample_F( wo, wi, p_sampler->Get2D(), &pdf, type );
    
    const vec3& n = surfInt.m_shading.n;
    // F_TODO:: CHECK PBRT
    // float nDotWi = CLAMP( dot( -1.f*n, wi ), 0.0f, 1.0f );
    float nDotWi = absDot( n, wi );

    if ( pdf > 0.f && nDotWi != 0.f && ( f.x != 0.f && f.y != 0.f && f.z != 0.f ) )
    {
        Ray newRay = surfInt.CreateRay( wi );

        return f * Li( newRay, p_scene, p_sampler, p_memArena, depth + 1 ) * nDotWi / pdf;
    }
    else
    {
        return {};
    }
}

//
//  WHITTED INTEGRATOR
//

vec3
WhittedIntegrator::Li(    
    const Ray& ray,
    const Scene* p_scene,
    ISampler* p_sampler,
    IMemAlloc* p_memArena,
    int depth ) const
{
    vec3 radiance = {};

    SurfaceInteraction surfInt;
    if ( p_scene->Intersect( ray, surfInt ) )
    {
        surfInt.ComputeScatteringFunctions( p_memArena, true, true );

        vec3 wo = surfInt.m_wo;

        //  If we've hit an area light accumulate emission from that surface
        //
        radiance += surfInt.GetAreaLightEmission( wo );

        for ( const auto* p_light : p_scene->m_lights )
        {
            vec3 wi {};
            float pdf {};
            VisibilityTester visTest;

            // vec3 irradiance = p_light->Sample_Li( surfInt, {} /*sample*/, wi, pdf, visTest );
            // vec3 irradiance = p_light->Sample_Li( surfInt, p_sampler->Get2DSample(), wi, pdf, visTest );
            vec3 irradiance = p_light->Sample_Li( surfInt, p_sampler->Get2D(), wi, pdf, visTest );
            if ( pdf == 0.f )
            {
                continue;
            }

            vec3 f = surfInt.m_bsdf->F( wo, wi );

            if ( visTest.Unoccluded( *p_scene ) )
            {
                radiance += f * irradiance * absDot( wi, surfInt.m_normal ) / pdf;
            }
        }

        if ( depth + 1 < m_maxBounces )
        {
            radiance += this->SpecularReflect( ray, surfInt, p_scene, p_sampler, p_memArena, depth );
            radiance += this->SpecularTransmit( ray, surfInt, p_scene, p_sampler, p_memArena, depth );
        }
    }
    else
    {
        vec3 dir = normalize( ray.m_dir );
        float t = 0.5f * ( dir.y + 1.0f );
        radiance += Vec3_Lerp( vec3( 0.5f, 0.7f, 1.0f ), vec3( 1.0f, 1.0f, 1.0f ), t );
    }

    return radiance;
}

//
//

//  Hardcoded with exponent = 2
//
inline float PowerHeuristic(int nf, float fPdf, int ng, float gPdf) {
    float f = nf * fPdf, g = ng * gPdf;
    return (f * f) / (f * f + g * g);
}

vec3
EstimateDirect(
    const Interaction& ref,
    vec2 uScatter,
    const ILight* p_light,
    vec2 uLight,
    const Scene* p_scene,
    ISampler*    p_sampler,
    IMemAlloc*  p_memArena,
    bool handleSpecular = false, bool handleMedia = false )
{
    vec3 irradiance     = {};
    BxdfType bsdfFlags  = handleSpecular ? kBxdfType_All : BxdfType( kBxdfType_All & ~kBxdfType_Specular );

    vec3    wi          = {};
    float   lightPdf    = 0.f;
    float   scatterPdf  = 0.f;

    VisibilityTester visTester;

    vec3 Li = p_light->Sample_Li( ref, uLight, wi, lightPdf, visTester );
    if ( lightPdf > 0.f && !VEC3_IS_ZERO( Li ) )
    {
        vec3 F {};
        if ( ref.isSurfaceInteraction() )
        {
            auto& surfIntx = ( const SurfaceInteraction& ) ref;
            F = surfIntx.m_bsdf->F( surfIntx.m_wo, wi, bsdfFlags ) * absDot( wi, surfIntx.m_shading.n );
            scatterPdf = surfIntx.m_bsdf->Pdf( surfIntx.m_wo, wi, bsdfFlags );
        }
        else
        {
            // F_TODO:: Medium interaction, need to handle this
            // F_TODO::[Volumes]
        }

        if ( !VEC3_IS_ZERO( F ) )
        {
            if ( handleMedia )
            {
                // F_TODO:: Do this part (Transmittance)
                // F_TODO::[Volumes]
            }
            else if ( !visTester.Unoccluded( *p_scene ) )
            {
                Li = {};    // Path is obstructed, zero out irradiance from p_light
            }

            if ( !VEC3_IS_ZERO( Li ) )
            {
                if ( p_light->IsDeltaLight() )
                {
                    irradiance += F * Li / lightPdf;
                }
                else
                {
                    float weight = PowerHeuristic( 1, lightPdf, 1, scatterPdf );
                    irradiance += F * Li * weight / lightPdf;
                }
            }
        }
    }

    //  Sample BSDF with multiple importance sampling
    //
    if ( !p_light->IsDeltaLight() )
    {
        vec3 F {};
        bool sampledSpecularBxdf = false;

        if ( ref.isSurfaceInteraction() )
        {
            BxdfType sampledType;
            auto& surfIntx = ( const SurfaceInteraction& ) ref;

            F = surfIntx.m_bsdf->Sample_F( surfIntx.m_wo, wi, uScatter, &scatterPdf, bsdfFlags, &sampledType );
            F *= absDot( wi, surfIntx.m_shading.n );
            sampledSpecularBxdf = (sampledType & kBxdfType_Specular) != 0;
        }
        else
        {
            // F_TODO:: Handle media interaction
            // F_TODO::[Volumes]
        }

        if ( !VEC3_IS_ZERO( F ) && scatterPdf > 0.f )
        {
            float weight = 1.f;
            
            if ( !sampledSpecularBxdf )
            {
                lightPdf = p_light->PDF_Li( ref, wi );
                if ( lightPdf == 0.f ) { return irradiance; }

                weight = PowerHeuristic( 1, scatterPdf, 1, lightPdf );
            }

            // F_TODO:: Compute transmittance
            SurfaceInteraction lightSurfInt;
            Ray ray = ref.CreateRay( wi );
            vec3 transmittance( 1.f );

            // F_TODO:: Handle transmittance conditionally (if handleMedia enabled)
            bool foundSurfaceInteraction = p_scene->Intersect( ray, lightSurfInt );

            vec3 Li {};
            if ( foundSurfaceInteraction )
            {
                if ( lightSurfInt.m_primPtr->GetAreaLight() == p_light ) 
                {
                    Li = lightSurfInt.GetAreaLightEmission( -1.f*wi );
                }
            }
            else
            {
                Li = p_light->Le( ray );
            }

            if ( !VEC3_IS_ZERO( Li ) )
            {
                irradiance += F * Li * transmittance * weight / scatterPdf;
            }
        }
        
    }

    return irradiance;
}


vec3
UniformSampleOneLight( 
    const Interaction&  ref,
    const Scene*        p_scene,
    IMemAlloc*          p_memArena,
    ISampler*            p_sampler,
    bool                handleMedia = false /* currently unsupported */ )
{
    size_t numLights = p_scene->m_lights.size();
    if ( numLights == 0u ) { return {}; }

    size_t lightIndex       = _MIN( numLights-1u, p_sampler->Get1D()*numLights );
    const ILight* p_light   = p_scene->m_lights[ lightIndex ];

    vec2 uLight             = p_sampler->Get2D();
    vec2 uScattering        = p_sampler->Get2D();

    return float(numLights) * EstimateDirect( ref, uScattering, p_light, uLight, p_scene, p_sampler, p_memArena, true );
}

//
//  DIRECT LIGHTING INTEGRATOR
//
vec3
DirectLightingIntegrator::Li(
    const Ray& ray,
    const Scene* p_scene,
    ISampler* p_sampler,
    IMemAlloc* p_memArena,
    int depth ) const
{
    vec3 radiance = {};

    SurfaceInteraction surfIntx;
    if ( p_scene->Intersect( ray, surfIntx ) )
    {
        surfIntx.ComputeScatteringFunctions( p_memArena, true, true );

        vec3 wo = surfIntx.m_wo;

        radiance += surfIntx.GetAreaLightEmission( wo );

        // Evaluate incident lighting
        if ( m_lightStrategy == LightStrategy::UniformSampleAll )
        {

        }
        else    // Uniform sample one light
        {
            radiance += UniformSampleOneLight( surfIntx, p_scene, p_memArena, p_sampler );
        }

        if ( depth + 1 < m_maxBounces )
        {
            radiance += this->SpecularReflect( ray, surfIntx, p_scene, p_sampler, p_memArena, depth );
            radiance += this->SpecularTransmit( ray, surfIntx, p_scene, p_sampler, p_memArena, depth );
        }
    }
    else
    {
        vec3 dir = normalize( ray.m_dir );
        float t = 0.5f * ( dir.y + 1.0f );
        radiance += Vec3_Lerp( vec3( 0.5f, 0.7f, 1.0f ), vec3( 1.0f, 1.0f, 1.0f ), t );
    }

    return radiance;
}

//
//  Path Tracing Integrator
//

vec3
PathTracingIntegrator::Li(
    const Ray& _ray,
    const Scene* p_scene,
    ISampler* p_sampler,
    IMemAlloc* p_memArena,
    int depth ) const
{
    vec3 irradiance {};

    vec3 throughput( 1.f );
    bool specularBounce = false;

    Ray ray = _ray;
    for ( int bounce = 0 ;; ++bounce )
    {
        SurfaceInteraction surfIntx;
        bool foundIntersection =  p_scene->Intersect( ray, surfIntx );
        vec3 wo = surfIntx.m_wo;

        if ( bounce == 0 )
        {
            if ( foundIntersection ) {
                irradiance += throughput * surfIntx.GetAreaLightEmission( wo );
            }
        }
        if ( !foundIntersection || bounce >= m_maxBounces ) { break; }

        surfIntx.ComputeScatteringFunctions( p_memArena, true, true );
        if ( !surfIntx.m_bsdf )
        {
            ray = surfIntx.CreateRay( ray.m_dir );
            bounce--;
            continue;
        }

        irradiance += throughput * UniformSampleOneLight( surfIntx, p_scene, p_memArena, p_sampler );

        //  Sample BSDF for next ray direction
        //
        vec3 wi;
        float pdf;
        BxdfType flags;
        vec3 F = surfIntx.m_bsdf->Sample_F( wo, wi, p_sampler->Get2D(), &pdf, kBxdfType_All, &flags );
 
        if ( VEC3_IS_ZERO( F ) || pdf == 0.f )
        {
            break;
        }

        specularBounce = (flags & kBxdfType_Specular ) != 0;
        bool transmissionBounce = (flags & kBxdfType_Transmission ) != 0;

        // FW_TODO:: Need to fix this, maybe in SurfaceInteraction::CreateRay()...
        //
        if ( transmissionBounce && ( dot( wi, surfIntx.m_normal ) < 0.0f )) {
            surfIntx.m_normal *= -1.0f;
            surfIntx.m_shading.n *= -1.0f;
        }

        throughput = throughput * F * absDot( wi, surfIntx.m_shading.n ) / pdf;

        ray = surfIntx.CreateRay( wi );

        //  Perform russian roulette
        //
        if ( bounce > 4 )
        {
            float q = _MAX( 0.05f, 1.f - throughput.x );
            if ( p_sampler->Get1D() < q )
            {
                break;
            }

            throughput /= 1.f - q;
        }
    }

    return irradiance;
}