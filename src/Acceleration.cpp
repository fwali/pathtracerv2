#include <algorithm>

#include "lib/Global.h"
#include "lib/Memory.h"

#include "Acceleration.h"


struct BVHPrimInfo
{
    AABB    m_bounds        {};
    vec3    m_centroid      {};
    size_t  m_primIndex     {};

    BVHPrimInfo() = default;

    BVHPrimInfo( size_t primIndex, const AABB& bounds )
    :
        m_bounds( bounds ),
        m_centroid( AABB_Centroid( bounds ) ),
        m_primIndex( primIndex )
    {}
};

struct BVHBuildNode
{
    AABB            bounds;
    BVHBuildNode*   p_children[ 2 ];
    int             splitAxis;
    size_t          firstPrimOffset;
    size_t          nPrims;

    void InitLeaf( size_t first, size_t count, const AABB& bbox )
    {
        bounds          = bbox;
        firstPrimOffset = first;
        nPrims          = count;
        p_children[ 0 ] = p_children[ 1 ] = nullptr;
    }

    void InitInterior( int axis, BVHBuildNode* p_c0, BVHBuildNode* p_c1 )
    {
        splitAxis       = axis;
        // firstPrimOffset = 0;
        nPrims          = 0;
        p_children[ 0 ] = p_c0;
        p_children[ 1 ] = p_c1;

        bounds = AABB_Union( p_c0->bounds, p_c1->bounds );
    }
};

BVHBuildNode*
BVH_BuildRecursive(
    StackAlloc& alloc,
    std::vector< BVHPrimInfo >& primInfo,
    size_t start, size_t end,
    size_t* o_totalNodes,
    BVHSplitMethod splitMethod,
    const std::vector< IPrimitive* >& prims,
    std::vector< IPrimitive* >& o_orderedPrims )
{
    BVHBuildNode* p_node = ALLOC_NO_INIT( alloc, BVHBuildNode );
    (*o_totalNodes)++;

    AABB bounds;
    for ( size_t i = start; i < end; ++i ) {
        bounds = AABB_Union( bounds, primInfo[ i ].m_bounds );
    }

    size_t nPrims = end - start;
    if ( nPrims == 1 )  // We're at a leaf node!
    {
        size_t firstPrimOffset = o_orderedPrims.size();
        for ( size_t i = start; i < end; ++i )
        {
            size_t primNum = primInfo[ i ].m_primIndex;
            o_orderedPrims.push_back( prims[ primNum ] );
        }

        p_node->InitLeaf( firstPrimOffset, nPrims, bounds );
    }
    else
    {
        AABB centroidBounds;
        for ( size_t i = start; i < end; ++i )
        {
            AABB_Expand( centroidBounds, primInfo[ i ].m_centroid );
        }

        int dim = AABB_MaxDim( centroidBounds );

        size_t mid = (start + end)/2;
        if ( centroidBounds.m_min.v[ dim ] == centroidBounds.m_max.v[ dim ] )
        {
            size_t firstPrimOffset = o_orderedPrims.size();
            for ( size_t i = start; i < end; ++i )
            {
                size_t primNum = primInfo[ i ].m_primIndex;
                o_orderedPrims.push_back( prims[ primNum ] );
            }

            p_node->InitLeaf( firstPrimOffset, nPrims, bounds );
        }
        else
        {
            //  Apply the appropriate split method to the range of prims we're operating on
            //
            switch( splitMethod )
            {
                case BVHSplitMethod::Middle:
                {
                    float pMid = ( centroidBounds.m_min.v[ dim ] + centroidBounds.m_max.v[ dim ] ) /2.f;
                    BVHPrimInfo* midPtr = 
                        std::partition( &primInfo[ start ], &primInfo[ end-1 ]+1, 
                            //  Lambda function to partition by midpoint
                            //
                            [ dim, pMid ] ( const BVHPrimInfo& pi ) {
                                return pi.m_centroid.v[ dim ] < pMid;
                            });
                    
                    mid = midPtr - &primInfo[0];

                    if ( mid != start && mid != end ) { break; }
                }
                case BVHSplitMethod::EqualCounts:
                {
                    mid = ( start + end ) / 2;

                    std::nth_element( &primInfo[ start ], &primInfo[ mid ], &primInfo[ end - 1 ] + 1, 
                        [dim]( const BVHPrimInfo& a, const BVHPrimInfo& b ) {
                            return a.m_centroid.v[ dim ] < b.m_centroid.v[ dim ];
                        });

                }   break;

                case BVHSplitMethod::SAH:
                default:
                {
                    // ASSERT_UNREACHED( "Cannot build BVH using SAH" )

                } break;
            }

            p_node->InitInterior(
                dim,
                BVH_BuildRecursive( alloc, primInfo, start, mid, o_totalNodes, splitMethod, prims, o_orderedPrims ),
                BVH_BuildRecursive( alloc, primInfo, mid, end, o_totalNodes, splitMethod, prims, o_orderedPrims ) );
        }
    }

    return p_node;
}

BVH::BVH( int maxPrimsPerNode, BVHSplitMethod splitMethod, std::vector< IPrimitive* >&& prims )
:
    IAggregate(),
    m_maxPrimsPerNode( maxPrimsPerNode ),
    m_splitMethod( splitMethod ),
    m_prims( prims )
{
    //  Populate BVHPrimInfo for each primitive
    //
    size_t numPrims = m_prims.size();
    std::vector< BVHPrimInfo > primInfoArray( numPrims );

    //  F_TODO:: Can parallelise this
    //
    for ( size_t i = 0u; i < numPrims; ++i )
    {
        primInfoArray[i] = BVHPrimInfo( i, m_prims[ i ]->ComputeBBox() );
    }

    size_t totalNodes = 0u;
    size_t memBufferSize = MB( 100 ); // size of the temp memory buffer, in bytes
    void* memBuffer = AlignedMalloc( memBufferSize );
    StackAlloc alloc( memBuffer, memBufferSize );

    //  Primitives will be reordered afterward for more efficient traversal
    //
    std::vector< IPrimitive* > orderedPrims;
    BVHBuildNode* rootNode;

    //  F_TODO:: Implement HLBVH construction!
    //
    // if ( m_splitMethod == BVHSplitMethod::HLBVH )
    // {
    // }
    // else
    {
        rootNode = BVH_BuildRecursive( alloc, primInfoArray, 0, numPrims, &totalNodes, m_splitMethod, m_prims, orderedPrims );
    }
    m_prims.swap( orderedPrims );

    //  Allocate #totalNodes LinearBVHNode's
    //
    size_t linearBvhSizeBytes = totalNodes * sizeof( LinearBVHNode );
    m_bvhNodePtr = (LinearBVHNode*) AlignedMalloc( linearBvhSizeBytes ); // F_TODO:: Free this memory in destructor via AlignedFree( ... )

    size_t offset = 0;
    this->FlattenBVHTree( rootNode, &offset );

    AlignedFree( memBuffer );
}

size_t
BVH::FlattenBVHTree( BVHBuildNode* p_bvhTreeNode, size_t* io_offset )
{
    LinearBVHNode*  p_linearNode = &m_bvhNodePtr[ *io_offset ];
    p_linearNode->bounds = p_bvhTreeNode->bounds;
    size_t myOffset = (*io_offset)++;

    if ( p_bvhTreeNode->nPrims > 0 )    // leaf node
    {
        p_linearNode->primitivesOffset  = p_bvhTreeNode->firstPrimOffset;
        p_linearNode->nPrims            = p_bvhTreeNode->nPrims;
    }
    else    // interior node
    {
        p_linearNode->axis      = p_bvhTreeNode->splitAxis;
        p_linearNode->nPrims    = 0u;

        FlattenBVHTree( p_bvhTreeNode->p_children[ 0 ], io_offset );
        p_linearNode->secondChildOffset = FlattenBVHTree( p_bvhTreeNode->p_children[ 1 ], io_offset );
    }

    return myOffset;
}

AABB BVH::ComputeBBox() const
{
    return {};
}

bool BVH::Intersects( const Ray& ray, ScenePrimIntersection& primInt ) const
{
    bool hit = false;

    //  F_TODO:: Should cache this info on the ray itself?
    //  
    vec3 invDir = { 1.f/ray.m_dir.x, 1.f/ray.m_dir.y, 1.f/ray.m_dir.z };
    int dirIsNeg[ 3 ] = {
        invDir.x < 0, invDir.y < 0, invDir.z < 0
    };

    size_t toVisitOffset        = 0u;
    size_t currentNodeIndex     = 0u;
    size_t nodesToVisit[ 64 ]   = {};

    while ( true )
    {
        const LinearBVHNode* p_node = &m_bvhNodePtr[ currentNodeIndex ];

        if ( AABB_IntersectsRay( p_node->bounds, ray, invDir ) )
        {
            if ( p_node->nPrims > 0 )   // leaf node
            {
                size_t numPrims = p_node->nPrims;
                for ( size_t i = 0u; i < numPrims; ++i )
                {
                    if ( m_prims[ p_node->primitivesOffset + i ]->Intersects( ray, primInt ) )
                    {
                        hit = true;
                    }
                }

                if ( toVisitOffset == 0 ) { break; }
                currentNodeIndex = nodesToVisit[ --toVisitOffset ];
            }
            else    // interior node
            {
                if ( dirIsNeg[ p_node->axis ] )
                {
                    nodesToVisit[ toVisitOffset++ ] = currentNodeIndex + 1;
                    currentNodeIndex = p_node->secondChildOffset;
                }
                else
                {
                    nodesToVisit[ toVisitOffset++ ] = p_node->secondChildOffset;
                    ++currentNodeIndex;
                }
            }
        }
        else
        {
            if ( toVisitOffset == 0 ) { break; }
            currentNodeIndex = nodesToVisit[ --toVisitOffset ];
        }
    }

    return hit;
}

bool BVH::IntersectsP( const Ray& ray ) const
{
    ScenePrimIntersection primInt;

    //  F_TODO:: Should cache this info on the ray itself?
    //  
    vec3 invDir = { 1.f/ray.m_dir.x, 1.f/ray.m_dir.y, 1.f/ray.m_dir.z };
    int dirIsNeg[ 3 ] = {
        invDir.x < 0, invDir.y < 0, invDir.z < 0
    };

    size_t toVisitOffset        = 0u;
    size_t currentNodeIndex     = 0u;
    size_t nodesToVisit[ 64 ]   = {};

    while ( true )
    {
        const LinearBVHNode* p_node = &m_bvhNodePtr[ currentNodeIndex ];

        if ( AABB_IntersectsRay( p_node->bounds, ray, invDir ) )
        {
            if ( p_node->nPrims > 0 )   // leaf node
            {
                size_t numPrims = p_node->nPrims;
                for ( size_t i = 0u; i < numPrims; ++i )
                {
                    if ( m_prims[ p_node->primitivesOffset + i ]->Intersects( ray, primInt ) )
                    {
                        return true;
                    }
                }

                if ( toVisitOffset == 0 ) { break; }
                currentNodeIndex = nodesToVisit[ --toVisitOffset ];
            }
            else    // interior node
            {
                if ( dirIsNeg[ p_node->axis ] )
                {
                    nodesToVisit[ toVisitOffset++ ] = currentNodeIndex + 1;
                    currentNodeIndex = p_node->secondChildOffset;
                }
                else
                {
                    nodesToVisit[ toVisitOffset++ ] = p_node->secondChildOffset;
                    ++currentNodeIndex;
                }
            }
        }
        else
        {
            if ( toVisitOffset == 0 ) { break; }
            currentNodeIndex = nodesToVisit[ --toVisitOffset ];
        }
    }

    return false;
}

const Material*   BVH::GetMaterial() const
{
    return nullptr;
}

const IAreaLight* BVH::GetAreaLight() const
{
    return nullptr;
}
const IShape*     BVH::GetShape() const
{
    return nullptr;
}
