#pragma once

#include "Material.h"
#include "Shape.h"
#include "Lights.h"

struct Primitive;

struct ScenePrimIntersection : public IntersectionResult
{
    const IPrimitive*   m_primPtr       = nullptr;
    Material*           m_materialPtr   = nullptr;
};

struct IPrimitive
{
    virtual AABB ComputeBBox() const = 0;

    virtual bool Intersects( const Ray& ray, ScenePrimIntersection& surfInteraction ) const = 0;

    virtual const Material*     GetMaterial() const = 0;
    virtual const IAreaLight*   GetAreaLight() const = 0;
    virtual const IShape*       GetShape() const = 0;
};

struct IAggregate : public IPrimitive
{
    virtual bool IntersectsP( const Ray& ray ) const = 0;
};

struct GeoPrimitive : IPrimitive
{
    IShape*     m_shapePtr      = nullptr;
    Material*   m_materialPtr   = nullptr;
    IAreaLight* m_areaLightPtr  = nullptr;


    GeoPrimitive( IShape* p_shape, Material* p_mat, IAreaLight* p_areaLight = nullptr )
    :
        m_shapePtr( p_shape ),
        m_materialPtr( p_mat ),
        m_areaLightPtr( p_areaLight )
    {}

    bool Intersects( const Ray& ray, ScenePrimIntersection& surfInteraction ) const override
    {
        if ( m_shapePtr->Intersects( ray, surfInteraction ) )
        {
            surfInteraction.m_materialPtr = m_materialPtr;
            surfInteraction.m_primPtr = this;

            return true;
        }
        else
        {
            return false;
        }
    }

    AABB ComputeBBox() const override
    {
        return m_shapePtr->ComputeBBox();
    }

    const Material* GetMaterial() const override
    {
        return m_materialPtr;
    }

    const IAreaLight* GetAreaLight() const override
    {
        return m_areaLightPtr;
    }

    const IShape* GetShape() const override
    {
        return m_shapePtr;
    }
};

struct Scene
{
    // std::vector< GeoPrimitive >     m_primitives {}; // F_TODO:: CHange to IPrimitive pointer to acceleration structure
    IAggregate*                     m_primitiveContainer;
    std::vector< ILight* >          m_lights {};

    bool Intersect( const Ray& ray, SurfaceInteraction& o_surfInt ) const;
    bool IntersectsP( const Ray& ray ) const;
};

#ifdef FW_SCENE_IMPL

bool Scene::Intersect( const Ray& ray, SurfaceInteraction& o_surfInt ) const
{
    ScenePrimIntersection intRes {};
    bool intersectionFound = m_primitiveContainer->Intersects( ray, intRes );

    if (intersectionFound )
    {
        const IShape* p_shape = intRes.m_primPtr->GetShape();
        if ( p_shape )
        {
            p_shape->GetShadingInfo( o_surfInt, intRes );
        }

        o_surfInt.m_primPtr     = intRes.m_primPtr;
        o_surfInt.m_materialPtr = intRes.m_materialPtr;
        o_surfInt.m_wo          = -1.f*ray.m_dir;
    }

    return intersectionFound;
}

bool Scene::IntersectsP( const Ray& ray ) const
{
    return m_primitiveContainer->IntersectsP( ray );
}

#endif