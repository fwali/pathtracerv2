#pragma once

#include "lib/Camera.h"
#include "lib/Memory.h"

struct Scene;
// struct Sampler;
struct SurfaceInteraction;

struct Image
{
    u32     m_width     {};
    u32     m_height    {};
    float*  m_buffer = nullptr;

    static size_t GetAllocationSizeBytes( u32 width, u32 height )
    {
        return width * height * 3u * sizeof( float );
    }

    Image( u32 width, u32 height, void* bufferMemory )
    : m_width( width ), m_height( height ), m_buffer( (float*) bufferMemory )
    {}
};

struct IIntegrator
{
    virtual ~IIntegrator() {}
    virtual void Init( const Scene* p_scene, IMemAlloc* p_memArena ) {}
    virtual void Render( const Scene* p_scene, IMemAlloc* p_memArena ) = 0;
};


struct SamplerIntegrator : IIntegrator
{
    ISampler*   m_sampler;
    Camera      m_camera;
    Image*      m_renderImage;
    const u32   m_maxBounces;

    std::vector< StackAlloc > m_perThreadMemArena;
    std::vector< ISampler* >  m_perThreadSampler;

    SamplerIntegrator( ISampler* sampler, Camera cam, Image* image, u32 maxBounces = 4u )
    :   IIntegrator(),
        m_sampler( sampler ),
        m_camera( cam ),
        m_renderImage( image ),
        m_maxBounces( maxBounces ),
        m_perThreadMemArena()
    {}

    void Render( const Scene* pScene, IMemAlloc* p_memArena ) override;

    vec3 SpecularReflect(   const Ray& ray,
                            const SurfaceInteraction& surfInt,
                            const Scene* p_scene,
                            ISampler* p_sampler,
                            IMemAlloc* p_memArena,
                            int depth ) const;
    
    vec3 SpecularTransmit(  const Ray& ray,
                            const SurfaceInteraction& surfInt,
                            const Scene* p_scene,
                            ISampler* p_sampler,
                            IMemAlloc* p_memArena,
                            int depth ) const;

    virtual void Init( const Scene* p_scene, IMemAlloc* p_memArena ) override;

    virtual void Preprocess( const Scene* p_scene, ISampler* p_sampler ) {}

    //  Computes the incoming radiance for a ray fired from a pixel position on the camera
    // 
    virtual vec3 Li(    const Ray& ray,
                        const Scene* p_scene,
                        ISampler* p_sampler,
                        IMemAlloc* p_memArena,
                        int depth = 0 ) const = 0;
};

struct WhittedIntegrator : SamplerIntegrator
{
    WhittedIntegrator( ISampler* sampler, Camera cam, Image* p_image )
    :
        SamplerIntegrator( sampler, cam, p_image )
    {}

    //  Computes the inco
    // 
    vec3 Li(    const Ray& ray,
                const Scene* p_scene,
                ISampler* p_sampler,
                IMemAlloc* p_memArena,
                int depth = 0 ) const override;
};

enum class LightStrategy
{
    UniformSampleAll,
    UniformSampleOne
};

struct DirectLightingIntegrator : SamplerIntegrator
{
    LightStrategy   m_lightStrategy;

    DirectLightingIntegrator( ISampler* sampler, Camera cam, Image* p_image, LightStrategy lightStrategy, u32 maxBounces = 4 )
    :
        SamplerIntegrator( sampler, cam, p_image, maxBounces ),
        m_lightStrategy( lightStrategy )
    {}

    //  Computes the incoming radiance for a ray
    // 
    vec3 Li(    const Ray& ray,
                const Scene* p_scene,
                ISampler* p_sampler,
                IMemAlloc* p_memArena,
                int depth = 0 ) const override;
};

struct PathTracingIntegrator : SamplerIntegrator
{
    PathTracingIntegrator( ISampler* p_sampler, Camera cam, Image* p_image, u32 maxBounces = 8 )
    :
        SamplerIntegrator( p_sampler, cam, p_image, maxBounces )
    {}


    //  Computes the incoming radiance for a ray
    // 
    vec3 Li(    const Ray& ray,
                const Scene* p_scene,
                ISampler* p_sampler,
                IMemAlloc* p_memArena,
                int depth = 0 ) const override;
};