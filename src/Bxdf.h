#pragma once

#include "lib/Datastructures.h"
#include "lib/Math.h"

#include "Spectrum.h"
#include "Sampling.h"

//
//  BXDFs
//

enum BxdfType : u32
{
    kBxdfType_Reflection    = BIT(1),
    kBxdfType_Transmission  = BIT(2),
    kBxdfType_Diffuse       = BIT(3),
    kBxdfType_Glossy        = BIT(4),
    kBxdfType_Specular      = BIT(5),

    kBxdfType_All = kBxdfType_Reflection |
                    kBxdfType_Transmission |
                    kBxdfType_Diffuse |
                    kBxdfType_Glossy |
                    kBxdfType_Specular
};


//
//  Utility Methods
//

//  All vectors are in the reflection coordinate system
//  The Normal vector for the surface is the z-axis [0, 0, 1]
//  The X and Y axes are the tangent, and bitangent respectively
//
//  The parameterisation from the Cartesian domain to Spherical Coords
//  
//      x = r * sin( theta ) * cos( phi )
//      y = r * sin( theta ) * sin( phi )
//      z = r * cos( theta )
//
//      Where:
//          r       = radius of Sphere (has value of 1 in the reflection coordinate system)
//          theta   = angle between the Z and X axes [ 0, Pi ]
//          phi     = angle between the X and Y axes [ 0, 2*Pi )
//

inline float CosTheta( const vec3& w )      { return w.z; }
inline float Cos2Theta( const vec3& w )     { return w.z*w.z; }
inline float AbsCosTheta( const vec3& w )   { return _ABS( w.z ); }
inline float Sin2Theta( const vec3& w )     { return _MAX( 0.0f, 1.0f - Cos2Theta( w ) ); }

inline float SinTheta( const vec3& w )  { return sqrtf( Sin2Theta( w ) ); }
inline float TanTheta( const vec3& w )  { return SinTheta( w ) / CosTheta( w ); }
inline float Tan2Theta( const vec3& w ) { return Sin2Theta( w ) / Cos2Theta( w ); }

inline float CosPhi( const vec3& w ) {
    float sinTheta = SinTheta( w );
    return ( sinTheta == 0.0f ) ? 1.0f : CLAMP( w.x/sinTheta, -1.0f, 1.0f );
}

inline float SinPhi( const vec3& w ) {
    float sinTheta = SinTheta( w );
    return ( sinTheta == 0.0f ) ? 0.0f : CLAMP( w.y/sinTheta, -1.0f, 1.0f );
}

inline float Cos2Phi( const vec3& w ) {
    float cosPhi = CosPhi( w );
    return cosPhi * cosPhi;
}

inline float Sin2Phi( const vec3& w ) {
    float sinPhi = SinPhi( w );
    return sinPhi * sinPhi;
}

inline float CosDPhi(const vec3 &wa, const vec3 &wb) {
    // F_TODO:: CHECK PBRT
    return CLAMP(
            (wa.x * wb.x + wa.y * wb.y) / std::sqrt((wa.x * wa.x + wa.y * wa.y) * (wb.x * wb.x + wb.y * wb.y)),
            -1.f, 1.f);
}

inline vec3 _Reflect( const vec3& wo, const vec3& n ) {
    return -1.f*wo + 2.f*dot( wo, n)*n;
}

inline bool Refract( const vec3& wi, const vec3& n, float eta, vec3& o_wt )
{
    float cosThetaI = dot( n, wi );
    float sin2ThetaI = _MAX( 0.0f, 1.0f - cosThetaI*cosThetaI );
    float sin2ThetaT = eta * eta * sin2ThetaI;

    //  Total internal reflection!
    // 
    if ( sin2ThetaT >= 1.0f ) { return false; }

    float cosThetaT = std::sqrt( _MAX( 0.f, 1.f - sin2ThetaT ) );
    o_wt = normalize( -eta*wi + ( eta * cosThetaI - cosThetaT )*n );
    return true;
}

inline bool SameHemisphere( const vec3& a, const vec3& b )
{
    return (a.z * b.z) > 0.f;
}

inline vec3 FaceForward( vec3 v, vec3 n )
{
    return ( dot( v, n ) >= 0.f ) ? n : -1.f*n; 
}

//
//  Defines the abstract base class for BRDFs and BTDFs
//
struct Bxdf
{
    const BxdfType  m_type;

    Bxdf( BxdfType type ) : m_type( type ) {}
    virtual ~Bxdf() {}

    inline bool MatchesType( BxdfType type ) const
    {
        return ( m_type & type ) == m_type;
    }

    //  BXDF Args:
    //
    //  wo = outgoing lighting direction on surface
    //  wi = indicent lighting direction
    //

    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    virtual vec3 F( const vec3& wo, const vec3& wi ) const = 0;

    //  Returns the BRDF for a given viewing direction and stores the incident
    //   lighting direction in p_wi. Can use sample information to compute incident direction
    //
    virtual vec3 Sample_F(  const vec3& wo,
                            vec3*       p_wi,
                            const vec2& sample,
                            float*      p_pdf,
                            BxdfType*   p_sampledType = nullptr ) const;

    //  Compute hemispherical-directional refletance (Rho)
    //
    //  The integral of cosine-weighted F over the oriented hemisphere in the positive Z-axis
    //  

    //  Hemispherical-Directional reflectance
    //  Computes the integral of all irradiance reflecting towards wo
    //
    virtual vec3 Rho( const vec3& wo, int nSamples, const vec2* p_samples ) const;

    //  Hemispherical-Hemispherical reflectance
    //  Computes the average reflectance over all outgoing directions from the -
    //    hemisphere of incoming radiance
    //
    virtual vec3 Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2 ) const;

    //  Returns the PDF value for a pair of incoming and outgoing directions ( 0.0, 1.0 )
    //
    virtual float Pdf( const vec3& wo, const vec3& wi ) const;

};

//
//  Represents up to 8 BxDF lobes, and provides all the utility functions to sample up to 8 BxDF lobes
//
struct Bsdf
{
    vec3    m_geoNormal;

    vec3    m_shadingNormal;
    vec3    m_shadingS;
    vec3    m_shadingT;


    TArray< Bxdf*, 8 > m_bxdfs; 
    float   m_eta;  // Relative index of refraction

    ~Bsdf() {}
    Bsdf(   vec3 geoNormal,
            vec3 shadingNormal,
            vec3 dpdu,
            float eta = 1.f )
    :
    m_geoNormal( geoNormal ),
    m_shadingNormal( shadingNormal ),
    m_shadingS( normalize( dpdu ) ),
    m_shadingT( cross( shadingNormal, m_shadingS ) ),
    m_eta( eta )
    {}

    void Add( Bxdf* p_bxdf )
    {
        m_bxdfs.Push( p_bxdf );
    }

    size_t NumComponents( BxdfType flags = kBxdfType_All ) const;

    vec3 WorldToLocal( const vec3& v ) const
    {
        return {
            dot( v, m_shadingS ),
            dot( v, m_shadingT ),
            dot( v, m_shadingNormal )
        };
    }

    vec3 LocalToWorld( const vec3& v ) const
    {
        return {
            m_shadingS.x * v.x + m_shadingT.x * v.y + m_shadingNormal.x * v.z,
            m_shadingS.y * v.x + m_shadingT.y * v.y + m_shadingNormal.y * v.z,
            m_shadingS.z * v.x + m_shadingT.z * v.y + m_shadingNormal.z * v.z,
        };
    }

    vec3 F( const vec3& world_wo, const vec3& world_wi, BxdfType flags = kBxdfType_All ) const;

    vec3 Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2, BxdfType flags = kBxdfType_All ) const;

    vec3 Rho( const vec3& wo, int nSamples, const vec2* p_samples, BxdfType flags = kBxdfType_All ) const;

    vec3 Sample_F(  const vec3& world_wo,
                    vec3& o_world_wi,
                    const vec2& u,
                    float* p_pdf,
                    BxdfType flags = kBxdfType_All,
                    BxdfType* p_sampledTypes = nullptr ) const;

    float Pdf( const vec3& world_wo, const vec3& world_wi, BxdfType flags = kBxdfType_All ) const;
};

//
//  A wrapper that simply scales the contribution of a BxDF
//
struct ScaledBxdf : public Bxdf
{
    Bxdf*       m_bxdf;
    const vec3  m_scale;

    ScaledBxdf( Bxdf* bxdf, const vec3& scale ) :
        Bxdf( bxdf->m_type ),
        m_bxdf( bxdf ),
        m_scale( scale )
    {}

    vec3 F( const vec3& wo, const vec3& wi ) const override
    {
        return m_scale * m_bxdf->F( wo, wi );
    }

    vec3 Sample_F(  const vec3& wo,
                    vec3*       p_wi,
                    const vec2& sample,
                    float*      p_pdf,
                    BxdfType*   p_sampledType = nullptr ) const override
    {
        return m_scale * m_bxdf->Sample_F( wo, p_wi, sample, p_pdf, p_sampledType );
    }

    vec3 Rho( const vec3& wo, int nSamples, const vec2* p_samples ) const override
    {
        return m_scale * m_bxdf->Rho( wo, nSamples, p_samples );
    }

    vec3 Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2 ) const override
    {
        return m_scale * m_bxdf->Rho( nSamples, p_samples1, p_samples2 );
    }

    float Pdf( const vec3& wo, const vec3& wi ) const override
    {
        return m_bxdf->Pdf( wo, wi );
    }
};

struct LambertianReflection : public Bxdf
{
    const vec3 r;

    explicit LambertianReflection( const vec3& attenuation )
        :
        Bxdf( BxdfType( kBxdfType_Diffuse | kBxdfType_Reflection ) ),
        r( attenuation )
    {}

    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override
    {
        return r*INV_PI;
    }

    //  Compute hemispherical-directional refletance (Rho)
    //
    //  The integral of cosine-weighted F over the oriented hemisphere in the positive Y-axis
    //  
    //
    vec3 Rho( const vec3& wo, int nSamples, const vec2* p_samples ) const override
    {
        return r;
    }

    vec3 Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2 ) const override
    {
        return r;
    }
};

struct Fresnel
{
    virtual ~Fresnel();
    virtual Spectrum Evaluate( float cosI ) const = 0; // cosI = Cosine of incidence angle with normal
};

struct FresnelDielectric : Fresnel
{
    float m_etaI;   //  Index of refraction in incident medium
    float m_etaT;   //  Index of refection in transmitted medium

    FresnelDielectric( float etaIncident, float etaTransmitted )
    :
        Fresnel(),
        m_etaI( etaIncident ),
        m_etaT( etaTransmitted )
    {}

    ~FresnelDielectric() {};
    Spectrum Evaluate( float cosI ) const override;
};

struct FresnelConductor : Fresnel
{
    Spectrum m_etaI;   // Index of refraction in incident medium
    Spectrum m_etaT;   // Index of refraction of conductor
    Spectrum m_k;      // Absorption coefficieng of conductor

    FresnelConductor( Spectrum etaI, Spectrum etaConductor, Spectrum absorption )
    :
        Fresnel(),
        m_etaI( etaI ),
        m_etaT( etaConductor ),
        m_k( absorption )
    {}

    ~FresnelConductor() {};
    Spectrum Evaluate( float cosI ) const override;
};

struct FresnelNoOp : Fresnel
{
    FresnelNoOp() = default;
    Spectrum Evaluate( float cosI ) const override;
};

struct SpecularReflection : public Bxdf
{
    Spectrum    m_R;
    Fresnel*    m_fresnelPtr;

    SpecularReflection( Spectrum R, Fresnel* p_fresnel )
    :
        Bxdf( BxdfType( kBxdfType_Specular | kBxdfType_Reflection ) ),
        m_R( R ),
        m_fresnelPtr( p_fresnel )
    {}


    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override
    {
        return {};
    }

    //  Returns the BRDF for a given viewing direction and stores the incident
    //   lighting direction in p_wi. Can use sample information to compute incident direction
    //
    vec3 Sample_F(  const vec3& wo,
                    vec3*       p_wi,
                    const vec2& sample,
                    float*      p_pdf,
                    BxdfType*   p_sampledType = nullptr ) const;
    
    float Pdf( const vec3& wo, const vec3& wi ) const
    {
        return 0.f;
    }
};

struct SpecularTransmission : public Bxdf
{
    Spectrum    m_T;            // Scaling factor for specular transmission contribution
    float       m_etaI;         // IOR for incident medium
    float       m_etaT;         // IOR for transmission medium
    FresnelDielectric* m_fresnel;
    // const TransportMode m_transportMode; // F_TODO::[LightTracing]

    SpecularTransmission( vec3 transmissiveStrength, float etaI, float etaT, FresnelDielectric* p_fresnel )
    :
        Bxdf( BxdfType(  kBxdfType_Specular | kBxdfType_Transmission ) ),
        m_T( transmissiveStrength ),
        m_etaI( etaI ),
        m_etaT( etaT ),
        m_fresnel( p_fresnel )
    {}

 
    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override
    {
        return {};
    }

    //  Returns the BRDF for a given viewing direction and stores the incident
    //   lighting direction in p_wi. Can use sample information to compute incident direction
    //
    vec3 Sample_F(  const vec3& wo,
                    vec3*       p_wi,
                    const vec2& sample,
                    float*      p_pdf,
                    BxdfType*   p_sampledType = nullptr ) const;
    
    float Pdf( const vec3& wo, const vec3& wi ) const
    {
        return 0.f;
    }
};

//  A specular BxDF that modulates the reflection:transmission ratio via a Fresnel op
//
struct FresnelSpecular : public Bxdf
{
    Spectrum m_R;
    Spectrum m_T;

    float m_etaI;
    float m_etaT;

    FresnelDielectric* m_fresnel;
    // const TransportMode transportFromEye; // F_TODO::[LightTracing]

    FresnelSpecular( Spectrum R, Spectrum T, float etaI, float etaT, FresnelDielectric* p_fresnel )
    :
        Bxdf( BxdfType( kBxdfType_Specular | kBxdfType_Transmission | kBxdfType_Reflection ) ),
        m_R( R ),
        m_T( T ),
        m_etaI( etaI ),
        m_etaT( etaT ),
        m_fresnel( p_fresnel )
    {}

    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override
    {
        return {};
    }

    //  Returns the BRDF for a given viewing direction and stores the incident
    //   lighting direction in p_wi. Can use sample information to compute incident direction
    //
    vec3 Sample_F(  const vec3& wo,
                    vec3*       p_wi,
                    const vec2& sample,
                    float*      p_pdf,
                    BxdfType*   p_sampledType = nullptr ) const;
    
    float Pdf( const vec3& wo, const vec3& wi ) const
    {
        return 0.f;
    }    
};

struct MicrofacetDistribution
{
    const bool m_sampleVisibleNormals = false;

    explicit MicrofacetDistribution( bool sampleVisibleNormals )
    :
        m_sampleVisibleNormals( sampleVisibleNormals )
    {}

    //  Evaluate the distribution for a given half-angle vector
    //
    virtual float D( const vec3& wh ) const = 0;

    virtual float Lambda(  const vec3& w ) const = 0;

    //  Sample the Microfacet distribution for a given outgoing light direction wo
    //  
    //      wo = Outgoing light direction
    //      u  = a 2D random sample, (x, y) => x and y are in [0, 1)
    //
    virtual vec3 Sample_Wh( const vec3& wo, const vec2& u ) const = 0;

    float Pdf( const vec3& wo, const vec3& wh ) const;

    float G1(  const vec3& w ) const
    {
        return 1.f / ( 1.f + Lambda( w ) );
    }

    float G( const vec3& wo, const vec3& wi ) const
    {
        return 1.f / ( 1.f + Lambda( wo ) + Lambda( wi ) );
    }
};

//
//  Maps a normalised floating point value for roughness to alpha for microfacet distributions
//
//      roughness:  surface roughness value, [0, 1]
//
//  Returns: Equivalent alpha value to use for microfacet distributions
//
inline float Beckmann_RoughnessToAlpha( float roughness )
{
    roughness = _MAX( roughness, (float) 1e-3 );
    float x = std::log( roughness );
    return  1.62142f + 0.819955f * x + 0.1734f * x * x +
            0.0171201f * x * x * x + 0.000640711f * x * x * x * x;
}

struct BeckmannDistribution : MicrofacetDistribution
{
    const float m_alphaX;
    const float m_alphaY;

    //  For constructing an anisotropic Beckmann distribution
    //
    BeckmannDistribution( float alphaX, float alphaY, bool sampleVisibleArea = true )
    :
        MicrofacetDistribution( sampleVisibleArea ),
        m_alphaX( alphaX ),
        m_alphaY( alphaY )
    {}

    //  Single alpha value produces an isotropic Beckmann distribution
    //
    BeckmannDistribution( float alpha, bool sampleVisibleArea = true )
    :
        MicrofacetDistribution( sampleVisibleArea ),
        m_alphaX( alpha ),
        m_alphaY( alpha )
    {}

    //  Evaluate the distribution for a given half-angle vector
    //
    virtual float D( const vec3& wh ) const override;

    virtual float Lambda(  const vec3& w ) const override;

    //  Sample the Microfacet distribution for a given outgoing light direction wo
    //  
    //      wo = Outgoing light direction
    //      u  = a 2D random sample, (x, y) => x and y are in [0, 1)
    //
    virtual vec3 Sample_Wh( const vec3& wo, const vec2& u ) const override;
};

struct TrowbridgeReitzDistribution : MicrofacetDistribution
{
    const float m_alphaX;
    const float m_alphaY;

    //  For constructing an anisotropic Beckmann distribution
    //
    TrowbridgeReitzDistribution( float alphaX, float alphaY, bool sampleVisibleArea = true )
    :
        MicrofacetDistribution( sampleVisibleArea ),
        m_alphaX( alphaX ),
        m_alphaY( alphaY )
    {}

    //  Single alpha value produces an isotropic Beckmann distribution
    //
    TrowbridgeReitzDistribution( float alpha, bool sampleVisibleArea = true )
    :
        MicrofacetDistribution( sampleVisibleArea ),
        m_alphaX( alpha ),
        m_alphaY( alpha )
    {}

    //  Evaluate the distribution for a given half-angle vector
    //
    virtual float D( const vec3& wh ) const override;

    virtual float Lambda(  const vec3& w ) const override;

    //  Sample the Microfacet distribution for a given outgoing light direction wo
    //  
    //      wo = Outgoing light direction
    //      u  = a 2D random sample, (x, y) => x and y are in [0, 1)
    //
    virtual vec3 Sample_Wh( const vec3& wo, const vec2& u ) const override;
};


struct MicrofacetReflection : Bxdf
{
    vec3                        R;
    MicrofacetDistribution*     p_distribution;
    Fresnel*                    p_fresnel;


    MicrofacetReflection( vec3 R, MicrofacetDistribution* dist, Fresnel* fresnel )
    :
        Bxdf( BxdfType( kBxdfType_Glossy | kBxdfType_Reflection ) ),
        R( R ),
        p_distribution( dist ),
        p_fresnel( fresnel )
    {}

    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override;

    //  Returns the BRDF for a given viewing direction and stores the incident
    //   lighting direction in p_wi. Can use sample information to compute incident direction
    //
    vec3 Sample_F(  const vec3& wo,
                    vec3*       p_wi,
                    const vec2& sample,
                    float*      p_pdf,
                    BxdfType*   p_sampledType = nullptr ) const override;
    
    float Pdf( const vec3& wo, const vec3& wi ) const override;
};

//  A BRDF that models a thin glossy layer over a diffuse reflection surface
//  The two lobes are modulated by the fresnel response
//
struct FresnelBlend : Bxdf
{
    const vec3              m_Rd;  // Diffuse reflectance
    const vec3              m_Rs;  // Specular reflectance
    MicrofacetDistribution* m_microfacetDist;

    FresnelBlend( vec3 diffuse, vec3 spec, MicrofacetDistribution* p_microfacetDist )
    :
        Bxdf( BxdfType( kBxdfType_Glossy | kBxdfType_Reflection ) ),
        m_Rd( diffuse ),
        m_Rs( spec ),
        m_microfacetDist( p_microfacetDist )
    {}

    //  Evaluate BXDF for a given outgoing and incident lighting direction
    //
    vec3 F( const vec3& wo, const vec3& wi ) const override;

    // //  Returns the BRDF for a given viewing direction and stores the incident
    // //   lighting direction in p_wi. Can use sample information to compute incident direction
    // //
    // vec3 Sample_F(  const vec3& wo,
    //                 vec3*       p_wi,
    //                 const vec2& sample,
    //                 float*      p_pdf,
    //                 BxdfType*   p_sampledType = nullptr ) const override;
    
    // float Pdf( const vec3& wo, const vec3& wi ) const override;
};
