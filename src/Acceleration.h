#pragma once

#include "Scene.h"


enum class BVHSplitMethod
{
    SAH,        // Surface area heuristic
    HLBVH,      // Linear BVH nodes
    Middle,     // Split bounding boxes down the middle
    EqualCounts // Leave equal amount of prims in leaf nodes
};

struct LinearBVHNode
{
    AABB    bounds;
    union
    {
        u32 primitivesOffset;   // leaf node
        u32 secondChildOffset;  // interior node
    };

    u16     nPrims;     // 0 for interior nodes
    u8      axis;       // Split axis, 0 = X, 1 = Y, 2 = Z
    u8      padding;    // 1 byte padding to make struct 32 bytes
}; 

struct BVHBuildNode;

struct BVH : IAggregate
{
    const int                   m_maxPrimsPerNode;
    const BVHSplitMethod        m_splitMethod;
    std::vector< IPrimitive* >  m_prims;

    LinearBVHNode*              m_bvhNodePtr;

    BVH( int maxPrimsPerNode, BVHSplitMethod splitMethod, std::vector< IPrimitive* >&& prims );

    //  IPrimitive interface methods
    //
    AABB ComputeBBox() const override;
    bool Intersects( const Ray& ray, ScenePrimIntersection& surfInteraction ) const override;
    bool IntersectsP( const Ray& ray ) const override;
    const Material*     GetMaterial() const override;
    const IAreaLight*   GetAreaLight() const override;
    const IShape*       GetShape() const override;

    //  BVH helpers
    //
    size_t FlattenBVHTree( BVHBuildNode* p_bvhNode, size_t* io_offset );
};

// F_TODO:: Implemenmt HLBVH