#include "Bxdf.h"

//  TOC:
//      Section (1) : BXDF Implementation
//      Section (2) : BSDF Implementation
//      Section (3) : Various concerete BxDF Implementations
//                  (3.a) : Specular reflection 
//
//      Section (4) : Microfacet distribution implementations

//
//  Section (1):
//  BXDF Implementation
//

vec3 Bxdf::Sample_F(    const vec3& wo,
                        vec3*       p_wi,
                        const vec2& sample,
                        float*      p_pdf,
                        BxdfType*   p_sampledType ) const
{
    *p_wi = CosineSampleHemisphere( sample );
    if ( wo.z < 0.f ) { p_wi->z *= -1.f; }
    *p_pdf = this->Pdf( wo, *p_wi );
    return this->F( wo, *p_wi );
}

vec3 Bxdf::Rho( const vec3& wo, int nSamples, const vec2* p_samples ) const
{
     vec3 r = {};
     for ( int i = 0u; i < nSamples; ++i )
     {
        vec3 wi = {};

        float pdf = 0.f;
        vec3 f = this->Sample_F( wo, &wi, p_samples[ i ], &pdf );
        if ( pdf > 0.f )
        {
             r += f * AbsCosTheta( wi ) / pdf;
        }
     }
     return r/nSamples;
}

vec3 Bxdf::Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2 ) const
{
    vec3 r = {};
    for ( int i = 0u; i < nSamples; ++i )
    {
        vec3 wo, wi;

        wo = UniformSampleHemisphere( p_samples1[ i ] );
        float pdf_wo = UniformHemispherePdf();
        float pdf_wi = 0.f;

        vec3 f = this->Sample_F( wo, &wi, p_samples2[i], &pdf_wi );
        if ( pdf_wi > 0.f )
        {
            r += f * AbsCosTheta( wi )*AbsCosTheta( wo ) / ( pdf_wo * pdf_wi );
        }
    }

    return r/nSamples;
}

float Bxdf::Pdf( const vec3& wo, const vec3& wi ) const
{
    return SameHemisphere( wo, wi ) ? AbsCosTheta( wi ) * INV_PI : 0.f;
}

//
//  Section (2):
//  BSDF IMPLEMENTATION
//

size_t Bsdf::NumComponents( BxdfType flags ) const
{
    size_t matchingComps = 0u;

    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) )
        {
            ++matchingComps;
        }
    }

    return matchingComps;
}

vec3 Bsdf::F( const vec3& world_wo, const vec3& world_wi, BxdfType flags ) const
{
    vec3 f = {};

    vec3 wo = normalize( this->WorldToLocal( world_wo ) );
    vec3 wi = normalize( this->WorldToLocal( world_wi ) );

    bool reflect = ( dot( world_wo, m_geoNormal ) * dot( world_wi, m_geoNormal ) ) > 0.f;

    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) &&
             ( ( reflect && ( m_bxdfs[ i ]->m_type & kBxdfType_Reflection ) ) ||
             ( !reflect && ( m_bxdfs[ i ]->m_type & kBxdfType_Transmission ) ) )
             )
        {
            f += m_bxdfs[ i ]->F( wo, wi );
        }
    }

    return f;
}

//  F_TODO:: Document how this works
//  F_TODO:: Can optimise this by preparing a list of matching BxDF conponents only once instead of always scanning
//
vec3 Bsdf::Sample_F(    const vec3& world_wo,
                        vec3& o_world_wi,
                        const vec2& u,
                        float* p_pdf,
                        BxdfType flags,
                        BxdfType* p_sampledTypes ) const
{
    vec3 F {};

    size_t matchingComps = this->NumComponents( flags );
    if ( matchingComps == 0u )
    {
        *p_pdf = 0.f;
        if ( p_sampledTypes ) 
        {
            *p_sampledTypes = BxdfType( 0 );
        }

        return F;
    }
    

    //  We get given a 2D random uniform float as input to the BSDF sampling routine
    //  Divide the interval [0,1) into n-pieces, where n is the number of matching BXDFs to sample
    //  Associate each of these pieces, d_i, with the i'th matching BxDF to sample
    //  
    int comp = _MIN( u.x * matchingComps, matchingComps - 1u );
    int count = comp;
    Bxdf* p_bxdf = nullptr; // Pointer to the BxDF that we will sample
    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) && (count--) == 0 )
        {
            p_bxdf = m_bxdfs[ i ];
            break;
        }
    }

    //  Use u.x, [0, 1), to determine which d_i u.x belongs to, and use that BxDF for sampling
    //  remap the inerval for d_i: [ (i-1)/d, i/d ) => [ 0, 1 ) 
    //
    vec2 uRemap = { _MIN( u.x*float(matchingComps) - float(comp), 0.9999f) ,u.y };

    vec3 wo = this->WorldToLocal( world_wo );
    vec3 wi = {};

    if ( wo.z == 0.f ) { return F; }

    *p_pdf = 0.f;
    if ( p_sampledTypes ) { *p_sampledTypes = p_bxdf->m_type; }
    F = p_bxdf->Sample_F( wo, &wi, uRemap, p_pdf, p_sampledTypes );

    if ( (*p_pdf) == 0.f )
    {
        if ( p_sampledTypes ) { *p_sampledTypes = BxdfType( 0 ); }
        return F;
    }

    o_world_wi = this->LocalToWorld( wi );

    //  Average the PDF of all matching BxDF types given the wi wo params derived above
    //  
    if ( !(p_bxdf->m_type & kBxdfType_Specular) && matchingComps > 1u )
    {
        TARRAY_FOR_EACH( i, m_bxdfs )
        {
            if ( m_bxdfs[ i ] != p_bxdf && m_bxdfs[ i ]->MatchesType( flags ) )
            {
                *p_pdf += m_bxdfs[ i ]->Pdf( wo, wi );
            }
        }
    }
    if ( matchingComps > 1u ) {  *p_pdf /= float(matchingComps); }

    if ( !(p_bxdf->m_type & kBxdfType_Specular ) )
    {
        bool reflect = dot( o_world_wi, m_geoNormal ) * dot( world_wo, m_geoNormal ) > 0.f;
        F = {};

        TARRAY_FOR_EACH( i, m_bxdfs )
        {
            if ( m_bxdfs[ i ]->MatchesType( flags ) &&
                ((reflect && ( m_bxdfs[i]->m_type & kBxdfType_Reflection)) ||
                 (!reflect && ( m_bxdfs[i]->m_type & kBxdfType_Transmission))))
            {
                F += m_bxdfs[ i ]->F( wo, wi );
            }
        }
    }

    return F;
}

vec3 Bsdf::Rho( const vec3& wo, int nSamples, const vec2* p_samples, BxdfType flags ) const
{
    vec3 R = {};

    vec3 local_wo = this->WorldToLocal( wo );

    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) )
        {
            R += m_bxdfs[ i ]->Rho( local_wo, nSamples, p_samples );
        }
    }

    return R;
}

vec3 Bsdf::Rho( int nSamples, const vec2* p_samples1, const vec2* p_samples2, BxdfType flags ) const
{
    // reflectance
    vec3 R = {};

    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) )
            R += m_bxdfs[ i ]->Rho( nSamples, p_samples1, p_samples2 );
    }

    return R;
}

float Bsdf::Pdf( const vec3& world_wo, const vec3& world_wi, BxdfType flags ) const
{
    float pdf = 0.f;

    if ( m_bxdfs.Count() == 0u )
    {
        return pdf;
    }

    vec3 wo = WorldToLocal( world_wo );
    vec3 wi = WorldToLocal( world_wi );

    if ( wo.z == 0.f ) { return pdf; }

    size_t matchingComps =  0;
    TARRAY_FOR_EACH( i, m_bxdfs )
    {
        if ( m_bxdfs[ i ]->MatchesType( flags ) )
        {
            ++matchingComps;
            pdf += m_bxdfs[ i ]->Pdf( wo, wi );
        }
    }

    if ( matchingComps == 0 )
    {
        return pdf; 
    }
    else
    {
        return pdf / float(matchingComps);
    }

}

//
//  Section (3): BxDF Implementations!
//

//  
//  Section (3.a): Specular reflection
//


//  Computes the Fresnel response for dielectric materials given:
//  Args::
//      cosThetaI   = cosine of incidence angle (theta)
//      etaI        = index of refraction for incident medium
//      etaT        = index of refraction for transmitted medium
//
//  Return:: a normalised float, [0.0, 1.0], that gives the ratio of light 
//      that is reflected vs transmitted
//
float
Fresnel_Dielectric( float cosThetaI, float etaI, float etaT )
{
    cosThetaI = CLAMP( cosThetaI, -1.0f, 1.0f );
    bool entering = cosThetaI > 0.f;
    if ( !entering )
    {
        SWAP( etaI, etaT );
        cosThetaI = _ABS( cosThetaI );
    }

    float sinThetaI = sqrt( _MAX( 0.f, 1.0f - cosThetaI*cosThetaI ) );
    float sinThetaT = ( etaI / etaT ) * sinThetaI;

    if ( sinThetaT >= 1.f )
    {
        return 1;
    }

    float cosThetaT = sqrt( _MAX( 0.f, 1.0f - sinThetaT*sinThetaT ) );

    float rParallel =   ( ( etaT*cosThetaI ) - ( etaI*cosThetaT ) ) /
                        ( ( etaT*cosThetaI ) + ( etaI*cosThetaT ) );

    float rPerpendincular = ( ( etaI*cosThetaI ) - ( etaT*cosThetaT ) ) /
                            ( ( etaI*cosThetaI ) + ( etaT*cosThetaT ) );

    return 0.5f*( rParallel*rParallel + rPerpendincular*rPerpendincular );
}

// F_TODO:: Needs to be tested
//  Fresnel response at a dieletric/conductor interface
//
Spectrum
Fresnel_Conductor( float cosThetaI, Spectrum etaI, Spectrum etaT, Spectrum k )
{
    cosThetaI = CLAMP( cosThetaI, -1.f, 1.f );
    float cos2ThetaI = cosThetaI * cosThetaI;
    float sin2ThetaI = _MAX( 0.f, 1.f - cos2ThetaI );

    Spectrum eta = etaT / etaI;
    Spectrum eta_k = k / etaI;

    Spectrum eta2 = eta * eta;
    Spectrum eta2_k = eta_k * eta_k;

    Spectrum t0 = eta2 - eta2_k - sin2ThetaI;
    Spectrum a2_plus_b2 = V3_SQRT( t0 * t0 + 4 * eta2 * eta2_k );
    Spectrum t1 = a2_plus_b2 + cos2ThetaI;
    Spectrum a = V3_SQRT( 0.5f*( a2_plus_b2 + t0 ) );
    Spectrum t2 = 2.f * cosThetaI * a;
    Spectrum Rs = ( t1 - t2 ) / ( t1 + t2 );

    Spectrum t3 = cos2ThetaI * a2_plus_b2 + sin2ThetaI*sin2ThetaI;
    Spectrum t4 = t2 * sin2ThetaI;
    Spectrum Rp = Rs * ( t3 - t4 ) / ( t3 + t4 );

    return 0.5f*( Rs + Rp );
}

Fresnel::~Fresnel() {}

Spectrum FresnelDielectric::Evaluate( float cosI ) const
{
    return Fresnel_Dielectric( cosI, m_etaI, m_etaT );
}

Spectrum FresnelConductor::Evaluate( float cosI ) const
{
    return Fresnel_Conductor( cosI, m_etaI, m_etaT, m_k );
}

Spectrum FresnelNoOp::Evaluate( float cosI )  const
{
    return Spectrum( 1.f );
}

vec3
SpecularReflection::Sample_F(
    const vec3& wo,
    vec3*       p_wi,
    const vec2& sample,
    float*      p_pdf,
    BxdfType*   p_sampledType ) const
{
    *p_wi = vec3( -1.f*wo.x, -1.f*wo.y, wo.z );
    *p_pdf = 1.0f;  // Specular BxDF has a delta distribution as its PDF

    if ( p_sampledType )
    {
        *p_sampledType = BxdfType( kBxdfType_Specular | kBxdfType_Reflection );
    }

    return m_fresnelPtr->Evaluate( CosTheta( *p_wi ) ) * m_R / AbsCosTheta( *p_wi );
}

Spectrum
SpecularTransmission::Sample_F(
    const vec3& wo,
    vec3*       p_wi,
    const vec2& sample,
    float*      p_pdf,
    BxdfType*   p_sampledType ) const
{
    bool entering = CosTheta( wo ) > 0.f;
    float etaI = entering ? m_etaI : m_etaT;
    float etaT = entering ? m_etaT : m_etaI;

    if ( !Refract( wo, FaceForward( wo, vec3(0.f, 0.f, 1.f) ), etaI/etaT, *p_wi ) )
    {
        // Total internal reflection!
        //
        return Spectrum(0.f);
    }

    if ( p_sampledType )
    {
        *p_sampledType = BxdfType( kBxdfType_Specular | kBxdfType_Transmission );
    }

    *p_pdf = 1.f;
    vec3 ft = m_T * ( Spectrum( 1.f ) - m_fresnel->Evaluate( CosTheta( *p_wi ) ) );
    // if ( m_transportMode == Radiance ) // F_TODO::[LightTracing]
        // ft *= ( etaI * etaI ) / ( etaT * etaT );

    return ft / AbsCosTheta( *p_wi );
}
// #define BXDF_DEBUG
Spectrum
FresnelSpecular::Sample_F(
    const vec3& wo,
    vec3*       p_wi,
    const vec2& sample,
    float*      p_pdf,
    BxdfType*   p_sampledType ) const
{
    #ifdef BXDF_DEBUG
    std::cout <<"[fresnel specular::Sample_F() BEGIN]\n";
    std::cout <<"\trandom sample = " <<sample <<"\n";
    #endif
    float fresnel = Fresnel_Dielectric( CosTheta( wo ), m_etaI, m_etaT );
    Spectrum radiance = {};

    #ifdef BXDF_DEBUG
    std::cout <<"\tetaI = " <<m_etaI <<", etaT = " <<m_etaT <<" wo = " << wo <<" fresnel = " << fresnel <<"\n";
    #endif
    if ( sample.x < fresnel ) // Reflection event
    {
        // std::cout <<"\treflection event!\n";
        *p_wi = vec3( -1.f*wo.x, -1.f*wo.y, wo.z );
        *p_pdf = fresnel;

        radiance = fresnel * m_R / AbsCosTheta( *p_wi );

        if ( p_sampledType )
        {
            *p_sampledType = BxdfType( kBxdfType_Specular | kBxdfType_Reflection );
        }
    }
    else    // Transmission event
    {
        // std::cout <<"\ttransmission event!\n";
        bool entering = CosTheta( wo ) > 0.f;
        float etaI = entering ? m_etaI : m_etaT;
        float etaT = entering ? m_etaT : m_etaI;

        if ( !Refract( wo, FaceForward( wo, vec3(0.f, 0.f, 1.f) ), etaI/etaT, *p_wi ) )
        {
            // Total internal reflection!
            //
            return Spectrum(0.f);
        }

        *p_pdf = 1.f - fresnel;

        vec3 ft = m_T * ( Spectrum( 1.f - fresnel ) );
        // if ( m_transportMode == Radiance ) // F_TODO::[LightTracing]
            ft *= ( etaI * etaI ) / ( etaT * etaT );

        radiance = ft / AbsCosTheta( *p_wi );

        if ( p_sampledType )
        {
            *p_sampledType = BxdfType( kBxdfType_Specular | kBxdfType_Transmission );
        }
    }

    #ifdef BXDF_DEBUG
    std::cout <<"\texitant radiance = " <<radiance <<"\n";
    std::cout <<"[fresnel specular::Sample_F() END]\n";
    #endif

    return radiance;
}

//
//  Section (4)
//  Microfacet Distribution Implementations
//

// 
//  Beckmann Distribution
//
//              e^( -Tan2(theta)( Cos2(phi)/a_x*a_x + Sin2(phi)/a_y*a_y ) )
//  D( wh ) =  --------------------------------------------------------------
//                            PI * a_x * a_y * Cos4( theta )
//

//  Evaluate the distribution for a given half-angle vector
//
float
BeckmannDistribution::D( const vec3& wh ) const
{
    float tan2Theta = Tan2Theta( wh );
    if ( std::isinf( tan2Theta) ) return 0.f;
    float cos4Theta = Cos2Theta( wh ) * Cos2Theta( wh );

    return std::exp( -tan2Theta*( Cos2Phi( wh)/(m_alphaX*m_alphaX) + Sin2Phi(wh)/( m_alphaY*m_alphaY ) ) ) /
            ( PI * m_alphaX * m_alphaY * cos4Theta );
}

float
BeckmannDistribution::Lambda(  const vec3& w ) const
{
    float absTanTheta = _ABS( TanTheta( w) );
    if ( std::isinf( absTanTheta ) ) { return 0.f; }

    float alpha = std::sqrt( Cos2Phi( w )*m_alphaX*m_alphaX + Sin2Phi( w )*m_alphaY*m_alphaY );

    float a = 1.f / ( alpha * absTanTheta );
    if ( a > 1.6f ) { return 0.f; }

    return (1 - 1.259f * a + 0.396f * a * a) /
           (3.535f * a + 2.181f * a * a);
}

//  Sample the Microfacet distribution for a given outgoing light direction wo
//  
//      wo = Outgoing light direction
//      u  = a 2D random sample, (x, y) => x and y are in [0, 1)
//
vec3
BeckmannDistribution::Sample_Wh( const vec3& wo, const vec2& u ) const
{
    if ( m_sampleVisibleNormals )
    {
        // F_TODO::
        return {};
    }
    else
    {
        float tan2Theta = 0.f;
        float phi       = 0.f;

        // if ( m_alphaX == m_alphaY )
        {
            float logSample = std::log( 1.f - u.x );
            tan2Theta = -m_alphaX*m_alphaX*logSample;
            phi = u.y*TAU;
        }
        // else    // anisotropic
        {
            // F_TODO::
        }

        float cosTheta  = 1.f / std::sqrt( 1.f + tan2Theta );
        float sinTheta  = std::sqrt( _MAX( 0.f, 1.f - cosTheta*cosTheta ) );
        vec3 wh = {
            std::sin( phi ) * cosTheta,
            std::sin( phi ) * sinTheta,
            std::cos( phi )
        };

        if ( !SameHemisphere( wo, wh ) ) { wh = -1.f*wh; }

        return wh;
    }
}


//  Evaluate the distribution for a given half-angle vector
//
float
TrowbridgeReitzDistribution::D( const vec3& wh ) const
{
    float tan2Theta = Tan2Theta( wh );

    if ( std::isinf( tan2Theta) ) return 0.f;

    float cos4Theta = Cos2Theta( wh ) * Cos2Theta( wh );

    float E = (Cos2Phi( wh )/(m_alphaX*m_alphaX) + Sin2Phi( wh )/(m_alphaY*m_alphaY)) * tan2Theta;
    return 1.f/ ( PI*m_alphaX*m_alphaY*cos4Theta*( 1.f + E )*( 1.f + E ) );
}

float
TrowbridgeReitzDistribution::Lambda(  const vec3& w ) const
{
    float absTanTheta = _ABS( TanTheta( w) );
    if ( std::isinf( absTanTheta ) ) { return 0.f; }

    float alpha = std::sqrt( Cos2Phi( w )*m_alphaX*m_alphaX + Sin2Phi( w )*m_alphaY*m_alphaY );

    float alpha2tan2Theta = alpha*alpha * absTanTheta*absTanTheta;

    return (-1.f + std::sqrt( 1 + alpha2tan2Theta ) )*0.5f;
}

float
MicrofacetDistribution::Pdf( const vec3& wo, const vec3& wh ) const
{
    if ( m_sampleVisibleNormals )
    {
        return D( wh ) * G1( wo ) * absDot( wo, wh ) / AbsCosTheta( wo );
    }
    else
    {
        return D( wh ) * AbsCosTheta( wh );
    }
}

//  Sample the Microfacet distribution for a given outgoing light direction wo
//  
//      wo = Outgoing light direction
//      u  = a 2D random sample, (x, y) => x and y are in [0, 1)
//
vec3
TrowbridgeReitzDistribution::Sample_Wh( const vec3& wo, const vec2& u ) const
{
    return {}; // F_TODO:: Look at Eric Heitz's paper on sampling VNDF for GGX
}

//  Evaluate BXDF for a given outgoing and incident lighting direction
//
vec3
MicrofacetReflection::F( const vec3& wo, const vec3& wi ) const
{
    float cosThetaO = AbsCosTheta( wo );
    float cosThetaI = AbsCosTheta( wi );

    vec3 wh = wo + wi;

    //  Handle degenerate cases
    //
    if ( cosThetaO == 0.f || cosThetaI == 0.f ) { return {}; }
    if ( wh.x == 0.f && wh.y == 0.f && wh.z == 0.f ) { return {}; }

    wh = normalize( wh );

    vec3 fresnel = Fresnel_Dielectric( cosThetaI, 1.f, 1.5f );
    return ( R * p_distribution->D( wh ) * p_distribution->G( wo, wi ) * fresnel ) /
            ( 4.f * cosThetaO * cosThetaI );
}

//  Returns the BRDF for a given viewing direction and stores the incident
//   lighting direction in p_wi. Can use sample information to compute incident direction
//
vec3
MicrofacetReflection::Sample_F(
    const vec3& wo,
    vec3*       p_wi,
    const vec2& u,
    float*      p_pdf,
    BxdfType*   p_sampledType ) const
{
    vec3 wh = p_distribution->Sample_Wh( wo, u );
    *p_wi = _Reflect( wo, wh );
    if ( !SameHemisphere( wo, *p_wi ) ) { return {}; }

    *p_pdf = p_distribution->Pdf( wo, wh ) /
                ( 4.f*dot( wo, wh ) );
    

    return this->F( wo, *p_wi );
}

float 
MicrofacetReflection::Pdf( const vec3& wo, const vec3& wi ) const
{
    if ( !SameHemisphere( wo, wi ) )    { return 0.f; }

    vec3 wh = normalize( wo + wi );
    return p_distribution->Pdf( wo, wh ) /
            ( 4.f*dot( wo, wh ) );
} 

//
//  Fresnel Blend Impl.
//

//  Evaluate BXDF for a given outgoing and incident lighting direction
//
vec3
FresnelBlend::F( const vec3& wo, const vec3& wi ) const
{
    vec3 f {};

    auto pow5 = [](float v) { return (v*v) * (v*v) * v; };

    vec3 diffuse = (28.f/(23.f*PI))*m_Rd*
                    (vec3(1.0) - m_Rs)*
                    ( 1.f - pow5( 1.f - 0.5f*AbsCosTheta( wi ) ) )*
                    ( 1.f - pow5( 1.f - 0.5f*AbsCosTheta( wo ) ) );


    vec3 wh = wo + wi;
    if ( VEC3_IS_ZERO( wh ) ) { return f; }
    wh = normalize( wh );

    float wiDotWh = dot( wi, wh );
    vec3 fresnel = m_Rs * pow5( 1.f - wiDotWh )*( vec3(1.0f) - m_Rs );

    vec3 spec = m_microfacetDist->D( wh ) /
                ( 4.f * absDot( wi, wh ) *
                _MAX( AbsCosTheta( wi ), AbsCosTheta( wo ) )) *
                fresnel;

    f = diffuse + spec;

    f = fresnel;

    return f;
}

// //  Returns the BRDF for a given viewing direction and stores the incident
// //   lighting direction in p_wi. Can use sample information to compute incident direction
// //
// vec3
// FresnelBlend::Sample_F( 
//     const vec3& wo,
//     vec3*       p_wi,
//     const vec2& u,
//     float*      p_pdf,
//     BxdfType*   p_sampledType ) const
// {
//     vec3 wh = m_microfacetDist->Sample_Wh( wo, u );
//     *p_wi = _Reflect( wo, wh );
//     if ( !SameHemisphere( wo, *p_wi ) ) { return {}; }

//     *p_pdf = m_microfacetDist->Pdf( wo, wh ) /
//                 ( 4.f*dot( wo, wh ) );
    

//     return this->F( wo, *p_wi );
// }

// float
// FresnelBlend::Pdf( const vec3& wo, const vec3& wi ) const
// {
//     if ( !SameHemisphere( wo, wi ) )    { return 0.f; }

//     vec3 wh = normalize( wo + wi );
//     return m_microfacetDist->Pdf( wo, wh ) /
//             ( 4.f*dot( wo, wh ) );
// }
