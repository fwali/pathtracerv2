#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>

#include <SDL2/SDL.h>

#ifdef _MSC_VER
#include <intrin.h>
#else
#include <x86intrin.h>
#endif

#define MATH_IMPLEMENTATION
#define CAMERA_IMPLEMENTATION
#define MEMORY_IMPLEMENTATION
#define PROFILE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#define FW_TASK_V2_IMPL


#include "lib/Global.h"
#include "lib/TaskV2.h"
#include "lib/Memory.h"
#include "lib/Math.h"
#include "lib/Camera.h"
#include "lib/Profile.h"
#include "lib/stb_image.h"
#include "tiny_obj_loader.h"

#define FW_SCENE_IMPL
#define FW_MATERIAL_IMPL
#define FW_SHAPES_IMPL
#define FW_LIGHTS_IMPL
#define FW_INTERACTIONS_IMPL

#include "Interactions.h"
#include "Lights.h"
#include "Scene.h"
#include "Material.h"
#include "Shape.h"
#include "Sampling.h"
#include "Bxdf.h"
#include "Integrator.h"
#include "Acceleration.h"
#include "RTTexture.h"

#include "Bxdf.cpp"
#include "Shape.cpp"
#include "Acceleration.cpp"
#include "Integrator.cpp"
#include "Sampling.cpp"

#include <pbrtParser/Scene.h>

//
//  Generate command line arg parser
//
#define CL_ARG_TABLE( ARG ) \
    ARG( char*, scenePath, "", 1, Copy, "scenePath", "i", "Filepath for scene file to be rendered." ) \
    ARG( char*, imagePath, "renders/image.ppm", 1, Copy, "outputPath", "o", "Filepath for rendered image to be saved." ) \
    ARG( u32, numSamples, 32u, 1, ToU32, "numSamples", "ns", "Number of samples to use for path tracing." ) \
    ARG( u32, width, 640u, 1, ToU32, "width", "w", "Width of image (# pixels).") \
    ARG( u32, height, 480u, 1, ToU32, "height", "h", "Height of image (# pixels)." )

#include "lib/CmdLineArgsGen.h"

#define MAX_BOUNCES 4


ImageTexture* ImageTexture_LoadFromFile(
    const char* imgFilepath
)
{
    ImageTexture* pImgTex = nullptr;

    int width, height, numComponents;
	byte* pTexData = stbi_load( imgFilepath, &width, &height, &numComponents, 0);

    if ( pTexData == nullptr )
    {
        std::cout <<"[F_DEBUG] Couldn't load " <<imgFilepath <<"\n";
    }
    else
    {
        std::cout <<"[F_DEBUG] Succesfully loaded " <<imgFilepath <<"\n";
    }

    pImgTex = new ImageTexture( pTexData, width, height, numComponents );    

    return pImgTex;
}

struct ObjLoadResults
{
    size_t numVerts {};
    size_t numIndices {};
    size_t numTriangles {};

    vec3* p_verts       {};
    u32*  p_indices     {};  
    vec2* p_uvs         {};
    vec3* p_normals     {};
    vec3* p_tangents    {};
};

ObjLoadResults
CreateTriMeshFromObjFile( const char* objFilepath )
{
    ObjLoadResults meshInfo;

    tinyobj::attrib_t attrib;
    std::vector< tinyobj::shape_t > shapes;
    std::vector< tinyobj::material_t > materials;
    std::string warnings, errors;

    if ( tinyobj::LoadObj( &attrib, &shapes, &materials, &warnings, &errors, objFilepath ) )
    {
        std::cout <<"[F_DEBUG] Succesfully loaded model '" <<objFilepath <<"'\n";
        std::cout <<"\t\t#Shapes = " <<shapes.size() <<"\n";

        std::cout   <<"#tex coords  = " <<attrib.texcoords.size()/2 <<"\n"
                    <<"#normals     = " <<attrib.normals.size()/3 <<"\n";

        //  F_TODO:: Deal with memory better
        //  
        size_t numCVs = attrib.vertices.size()/3;
        vec3* newCVs = new vec3[ numCVs ];
        // u32*  newIndices = new u32[ numIndices ];
        std::vector< u32 > newIndicesVec;
        for ( const tinyobj::shape_t& shape : shapes )
        {
            std::cout <<"\t\tShape name: " <<shape.name <<"\n";

            size_t numIndices = shape.mesh.indices.size();
            std::cout <<"\t\tNum CVs, indices = " <<numCVs <<", " << numIndices <<"\n";


            float* pNewCvBegin = ( float* ) newCVs;
            for ( float f : attrib.vertices )
            {
                *pNewCvBegin = f;
                ++pNewCvBegin;
            }

            // u32* pNewIndexStart = newIndices;
            for ( const tinyobj::index_t& index : shape.mesh.indices )
            {
                newIndicesVec.push_back( ( u32 ) index.vertex_index );
                // ++pNewIndexStart;
                // std::cout <<index.vertex_index <<" ";
            }
            std::cout <<"\n";

        }
        size_t numIndices = newIndicesVec.size();
        size_t numTris = numIndices / 3u;
        u32* pNewIndexBuffer = new u32[ numIndices];
        memcpy( pNewIndexBuffer, newIndicesVec.data(), sizeof(u32)*numIndices );

        meshInfo.numVerts = numCVs;
        meshInfo.numIndices = numIndices;
        meshInfo.numTriangles = numTris;
        meshInfo.p_verts = newCVs;
        meshInfo.p_indices = pNewIndexBuffer;
    }
    else
    {
        std::cerr <<"[F_DEBUG] Couldn't load obj file '" <<objFilepath <<"'\n";
    }

    return meshInfo;
}

#define ARRAY_SIZE( A )   sizeof( A ) / sizeof( A[0] )

void Scene_Init( Scene* pScene, IMemAlloc* pMemAlloc )
{
#define FW_ALLOC( Type ) ALLOC_NEW( memAlloc, Type )

    IMemAlloc& memAlloc = *pMemAlloc;

    std::vector< IPrimitive* > scenePrims;

#if 0
    ImageTexture* imgTex        = ImageTexture_LoadFromFile( "images/brick_wall_albedo.jpg" );
    ImageTexture* imgTex2       = ImageTexture_LoadFromFile( "images/metalplate_albedo.jpg");

    auto* greenColourTex        = FW_ALLOC( FlatColour ) ( vec3( 0.0f, 0.8f, 0.0f ) );
    auto* blueColourTex         = FW_ALLOC( FlatColour ) ( vec3( 0.0f, 0.0f, 0.8f ) );
    FlatColour* redColourTex    = FW_ALLOC( FlatColour ) ( vec3( 0.8f, 0.0f, 0.0f ) );
    FlatColour* whiteColourTex  = FW_ALLOC( FlatColour ) ( vec3( 0.8f ) );
    FlatColour* greyColourTex   = FW_ALLOC( FlatColour ) ( vec3( 0.01f ) );
    auto*   pWaveTex            = FW_ALLOC( WaveTexture ) ( vec3( 0.f, 0.f, 1.4f ) );

    CheckerTexture* pCheckTex   = FW_ALLOC( CheckerTexture) ( { 0.42f, 0.42f, 0.42f }, { 0.8f, 0.85f, 0.8f } );

    Material* stdMtlGreen       = FW_ALLOC( MatteMaterial ) ( greenColourTex, nullptr  );
    Material* stdMtlBlue        = FW_ALLOC( MatteMaterial ) ( blueColourTex, nullptr );
    Material* stdMtlRed         = FW_ALLOC( MatteMaterial ) ( redColourTex, nullptr );
    Material* stdMtlWhite       = FW_ALLOC( MatteMaterial ) ( whiteColourTex, nullptr );
    Material* imgMtl            = FW_ALLOC( MatteMaterial ) ( imgTex, nullptr );
    Material* imgMtl2           = FW_ALLOC( MatteMaterial ) ( imgTex2, nullptr );
    // Material* pCheckMaterial    = ALLOC_NEW( memAlloc, MatteMaterial ) ( pCheckTex, nullptr );
    Material* pCheckMaterial    = FW_ALLOC( ReflectiveMaterial ) ( pCheckTex, 1.6f );
    Material* pGlassMaterial    = ALLOC_NEW( memAlloc, RefractiveMaterial ) ( 1.5f );
    // Material* pMicroFacetMat    = FW_ALLOC( MicrofacetMaterial ) ( pCheckTex,  greyColourTex, pCheckTex );
    Material* pMicroFacetMat    = FW_ALLOC( MicrofacetMaterial ) ( pCheckTex,  greyColourTex, pWaveTex );
    // Material* pMicroFacetMat    = FW_ALLOC( PlasticMaterial ) ( pCheckTex,  greyColourTex, pWaveTex );

    IShape* pSpheres[] = {
        FW_ALLOC( Shape_Sphere ) ( {0.0f, -1000.0f, 0.0f } , 997.5f, vec2( 6.0f ) ),    // floor
        FW_ALLOC( Shape_Sphere ) ( { 0.0f, 0.0f, 1000.0f } , 990.f, vec2( 120.0f ) ),       // back

        FW_ALLOC( Shape_Sphere ) ( {-10.0f, 0.0f, 0.0f } , 2.5f ),
        FW_ALLOC( Shape_Sphere ) ( {0.0f, 0.0f, 0.0f } , 2.5f ),

        FW_ALLOC( Shape_Sphere ) ( { -1020.f, 0.f, 0.f }, 1000.f, vec2( 100.0f ) ),
        FW_ALLOC( Shape_Sphere ) ( { 1020.f, 0.f, 0.f }, 1000.f, vec2( 100.0f ) )
        // FW_ALLOC( Shape_Sphere )( {10.0f, 0.5f, -1.0f } , 2.5f )
    };

    Material* pSphereMats[] = {
        // pCheckMaterial,
        // pMicroFacetMat,
        pCheckMaterial,
        // stdMtlWhite,
        imgMtl,

        imgMtl,
        imgMtl2,
        // stdMtlBlue
        // pGlassMaterial

        stdMtlGreen,
        stdMtlRed
    };

    size_t numSpheres = ARRAY_SIZE( pSpheres );
    for ( size_t i = 0u; i < numSpheres; ++i )
    {
        scenePrims.push_back( FW_ALLOC( GeoPrimitive ) ( pSpheres[ i ], pSphereMats[ i ] ) );
    }
#else

    auto* pPerfectWhiteTex = FW_ALLOC( FlatColour ) ( vec3( 1.f ) );
    auto* pWhiteTex = FW_ALLOC( FlatColour ) ( vec3( 0.8f, 0.8f, 0.8f ) );
    auto* pRedTex = FW_ALLOC( FlatColour ) ( vec3( 0.8f, 0.0f, 0.0f ) );
    auto* pGreenTex = FW_ALLOC( FlatColour ) ( vec3( 0.f, 0.8f, 0.f ) );
    auto* pBlueTex = FW_ALLOC( FlatColour ) ( vec3( 0.f, 0.f, 0.8f ) );

    Material* pMatWhite = FW_ALLOC( MatteMaterial ) ( pWhiteTex, nullptr );
    Material* pMatRed = FW_ALLOC( MatteMaterial ) ( pRedTex, nullptr );
    Material* pMatGreen = FW_ALLOC( MatteMaterial ) ( pGreenTex, nullptr );
    Material* pMatBlue = FW_ALLOC( MatteMaterial ) ( pBlueTex, nullptr );

    // Material* pFresnelSpec = FW_ALLOC( FresnelSpecMaterial ) ( pPerfectWhiteTex, pPerfectWhiteTex, 1.0f, 1.6f );
    Material* pFresnelSpec = FW_ALLOC( FresnelSpecMaterial ) ( pPerfectWhiteTex, pPerfectWhiteTex, 1.0f, 2.2f );

#endif

#define ADD_SPHERE_SHAPE( POS, RADIUS, MATERIAL ) \
    scenePrims.push_back( FW_ALLOC( GeoPrimitive ) ( \
        FW_ALLOC( Shape_Sphere ) ( POS, RADIUS, vec2(1.f) ), \
        MATERIAL \
    ))

#define ADD_SPHERICAL_AREA_LIGHT(POS, RADIUS, LIGHT_VALUE)                              \
    {                                                                                   \
        auto *p_shape = FW_ALLOC(Shape_Sphere)(POS, RADIUS);                            \
        auto *p_areaLight = FW_ALLOC(DiffuseAreaLight)(LIGHT_VALUE, 4, p_shape);        \
                                                                                        \
        scenePrims.push_back(FW_ALLOC(GeoPrimitive)(p_shape, pMatWhite, p_areaLight)); \
        pScene->m_lights.push_back(p_areaLight);                                        \
    }

    ADD_SPHERICAL_AREA_LIGHT( vec3( 0.f, 12.f, 0.f ), 6.0f, vec3( 10.f) )
    // ADD_SPHERICAL_AREA_LIGHT( vec3( 10.f, 0.5f, 0.f ), 2.5f, vec3( 0.f, 0.f, 10.f) )

#if 0
    ADD_SPHERE_SHAPE(  vec3( 0.0f, 0.0f, 1000.f), 990.5f, pMatBlue );
    ADD_SPHERE_SHAPE(  vec3( 0.0f, -1000.f, 0.0f ), 997.5f, pMatWhite );
    ADD_SPHERE_SHAPE(  vec3( 1000.f, 0.0f, 0.0f ), 980.f, pMatGreen );
    ADD_SPHERE_SHAPE(  vec3( -1000.f, 0.f, 0.f ), 980.f, pMatRed );

    // ADD_SPHERE_SHAPE(  vec3( 0.0, 0.0f, 0.0f ), 3.0f, pMatWhite );
    // ADD_SPHERE_SHAPE(  vec3( 0.0, 1.0f, 0.0f ), 3.0f, pFresnelSpec );
#else

    ADD_SPHERE_SHAPE( vec3( 0.0f, 1.0f, 0.0f ), 2.0f, pFresnelSpec );
    // ADD_SPHERE_SHAPE( vec3( 0.0f ), 1.0f, pMatWhite );

    ADD_SPHERE_SHAPE( vec3( 0.0f, -101.f, 0.0f ), 100.f, pMatGreen );
    ADD_SPHERE_SHAPE( vec3( 0.0f, 0.0f, 106.f ), 100.f, pMatBlue );

#endif

#if 0

#if 0
    Mat4x4 transform = CreateAffineTransform({ -2.0f, -2.0f, -2.0f }, 0.08f,
                            Quat_Rotation( { 0.f, 1.f, 0.f }, PI/4.f ) );
    ObjLoadResults objMesh = CreateTriMeshFromObjFile( "./models/teapot/teapot.obj" );
#else
    Mat4x4 transform = CreateAffineTransform( { -4.0f, -3.0f, -5.0f }, 0.01f, {} );
    ObjLoadResults objMesh = CreateTriMeshFromObjFile( "./models/lowpolydeer/deer.obj" );
#endif

    auto* p_triMesh = FW_ALLOC( TriangleMeshData ) (    objMesh.numTriangles, objMesh.numVerts,
                                                        objMesh.p_indices, objMesh.p_verts,
                                                        objMesh.p_uvs, objMesh.p_normals, objMesh.p_tangents,
                                                        transform );

    const size_t numTris = objMesh.numTriangles;
    u32* indices = objMesh.p_indices;

    AABB bbox = p_triMesh->m_bbox;
    std::cout <<"[F_DEBUG] Mesh bbox = ( "  <<bbox.m_min.x <<", " <<bbox.m_min.y <<", " <<bbox.m_min.z <<") - >  ("
                                            <<bbox.m_max.x <<", " <<bbox.m_max.y <<", " <<bbox.m_max.z <<")\n";


    for ( size_t i = 0u; i < numTris; ++i )
    {
        size_t indicesOffset = i*3u;
        TriangleShape* newShape = FW_ALLOC( TriangleShape ) ( p_triMesh, indices + indicesOffset );
        
        // pScene->m_primitives.push_back( { newShape, stdMtlRed } );
        scenePrims.push_back( FW_ALLOC( GeoPrimitive ) ( newShape, pFresnelSpec ) );
        // scenePrims.push_back( FW_ALLOC( GeoPrimitive ) ( newShape, stdMtlRed ) );
    }

#endif

    {
        PROFILE( BVH_Construction )

        BVH* sceneBVH = FW_ALLOC( BVH ) ( 10, BVHSplitMethod::Middle, std::move(scenePrims) );
        pScene->m_primitiveContainer = sceneBVH;
    }

#undef FW_ALLOC
}

//  A light-weight "integrator" that will fire a single ray into the scene at pixelPos
//  Will print out debug info for the ray as it propagates through the scene
//
void
PixelDebugger( Scene* scene, ISampler* p_sampler, Camera* camera, IMemAlloc* memAlloc, vec2 cameraDim, vec2 pixelPos )
{
    std::cout <<"[RUNNING PIXEL DEBUGGER]---------------------------\n\n";
    const int maxBounces = 5;
    // int bounce = 0;

    Ray pixelRay;
    CastRayFromCamera( camera, pixelPos, cameraDim, pixelRay );

    bool specularBounce = false;

    for ( int bounce = 0; bounce < maxBounces; ++bounce )
    {
        std::string indent( bounce, '\t' );
        std::cout <<indent <<"Bounce #" <<bounce<<"\n";
        std::cout <<indent <<"Specular bounce? " << (specularBounce ? "true" : "false") <<"\n";
        std::cout <<indent <<"Firing Ray: " <<pixelRay <<"\n";

        SurfaceInteraction surfIntx;
        bool foundIntersection = scene->Intersect( pixelRay, surfIntx );

        if ( not foundIntersection )
        {
            std::cout <<indent <<"NO INTERSCETION FOUND -------\n";
            goto endpixeldebugger;
        }

        std::cout <<indent <<"Found intersection!\n";
        std::cout <<indent <<"Interaction: pos = " << surfIntx.m_pos <<", normal = " << surfIntx.m_normal <<"\n";


        vec3 wo = surfIntx.m_wo;
        surfIntx.ComputeScatteringFunctions( memAlloc, true /* from eye */, true /* multiple lobes */ );

        if ( surfIntx.m_bsdf )
        {
            std::cout <<indent <<"Found BSDF: Sampling for next ray direction...\n";

            //  Sample BSDF for next ray direction
            //
            vec3 wi;
            float pdf;
            BxdfType flags;
            vec3 F = surfIntx.m_bsdf->Sample_F( wo, wi, p_sampler->Get2D(), &pdf, kBxdfType_All, &flags );

            specularBounce = ( flags & kBxdfType_Specular ) != 0;
            const bool transmissionBounce = ( flags & kBxdfType_Transmission ) != 0;

            if ( transmissionBounce && ( dot( wi, surfIntx.m_normal ) < 0.0f )) {
                surfIntx.m_normal *= -1.0f;
                surfIntx.m_shading.n *= -1.0f;
            }

            pixelRay = surfIntx.CreateRay( wi );

            std::cout <<indent <<"  wi = " << wi << " pdf = " << pdf 
                                <<" specular? " << (specularBounce ? "true" : "false" )
                                <<" transmission? " << (transmissionBounce ? "true" : "false")
                                <<"\n";
        }
        else
        {
            std::cout <<indent <<"No BSDF found. Quitting!\n";
            goto endpixeldebugger;
        }
    }

endpixeldebugger:
    std::cout <<"[FINISHED RUNNING PIXEL DEBUGGER]-------------------\n\n\n";
}

int main( int argc, char** argv )
{
    CmdLineArgs cmdLineArgs;
    ProcessCommandLineArgs( &cmdLineArgs, argc, argv ); 


    SDL_Window* p_sdlWindow = nullptr;
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "[ERROR] SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    }

    //  Allocate 1000 MB up front for various purposes
    //
    size_t stackSize = MB(1000);
    void* alignedMem = AlignedMalloc( stackSize, SSE_ALIGN );
    StackAlloc stackAlloc( alignedMem, stackSize );

    // Create an application window with the following settings:
    p_sdlWindow = SDL_CreateWindow(
        "Harmonic PT",                     // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        cmdLineArgs.width,                 // width, in pixels
        cmdLineArgs.height,                // height, in pixels
        SDL_WINDOW_OPENGL                  // flags - see below
    );

        // Check that the window was successfully created
    if (p_sdlWindow == nullptr) {
        // In the case that the window could not be made...
        printf("[ERROR] Could not create window: %s\n", SDL_GetError());
        return 1;
    }


    printf("Running Pathtracer\n");
    printf("Rendering with %u samples\n", cmdLineArgs.numSamples );

    size_t numSamples       = (size_t) cmdLineArgs.numSamples;
    float invSample         = 1.0f / ( (float) numSamples );
    size_t imageWidth       = (size_t) cmdLineArgs.width;
    size_t imageHeight      = (size_t) cmdLineArgs.height;
    size_t numPixels        = imageWidth * imageHeight;
    size_t numComponents    = numPixels * 3u;

    size_t renderBufferSizeBytes = Image::GetAllocationSizeBytes( imageWidth, imageHeight );
    Image renderBuffer( imageWidth, imageHeight, ALLOC_BYTES( stackAlloc, renderBufferSizeBytes  ) ); 
    float* pImageBuffer = renderBuffer.m_buffer;

    //  Turn the render buffer into an 8 bits per-pixel image
    //
    byte* pDisplayImageBuffer = ALLOC_ARRAY_NO_INIT( stackAlloc, byte, numComponents );

    // Set up the pixel format color masks for RGB(A) byte arrays.
    // Only STBI_rgb (3) and STBI_rgb_alpha (4) are supported here!
    Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    int shift = (req_format == STBI_rgb) ? 8 : 0;
    rmask = 0xff000000 >> shift;
    gmask = 0x00ff0000 >> shift;
    bmask = 0x0000ff00 >> shift;
    amask = 0x000000ff >> shift;
#else // little endian, like x86
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0; //(req_format == STBI_rgb) ? 0 : 0xff000000;
#endif

    SDL_Surface* renderSurface = SDL_CreateRGBSurfaceFrom(
                                    pDisplayImageBuffer,
                                    imageWidth,
                                    imageHeight,
                                    24 /* bits per pixel */,
                                    imageWidth * 3 /* bytes per row */,
                                    rmask, gmask, bmask, amask );

    //The surface contained by the window
    SDL_Surface* gScreenSurface = NULL;

    //Get window surface
    gScreenSurface = SDL_GetWindowSurface( p_sdlWindow );

    Camera renderCam;
    Scene* pScene = new Scene();

    vec2 cameraDim = { ( float ) imageWidth, (float) imageHeight };

    { 
        PROFILE( SceneInit )

        renderCam = Camera_Create(
                        // vec3( 6.f, 1.f, -30.f ) /*eye pos */,
                        // vec3( -3.f, 7.5f, -20.f ) /*eye pos */,
                        vec3( 0.0f, 2.0f, -10.0f ), 
                        vec3( 0.f, 0.f, 0.f ) /* target */,
                        vec3( 0.f, 1.f, 0.f ) /* up */, 
                        (float) imageWidth / (float) imageHeight /*aspect ratio*/,
                        45.f /* vertical FoV */,
                        0.1f /* near */, 1000.f /* far */ );

        // Scene_Init( pScene, &stackAlloc );

        pbrt::Scene::SP p_pbrtScene;
        const char* scenePath = cmdLineArgs.scenePath;
        if ( strstr( scenePath, ".pbrt" ) )
        {
            std::cout <<scenePath <<" is a pbrt file!\n";
            // p_pbrtScene = pbrt::impo
            p_pbrtScene = pbrt::importPBRT( scenePath );


        }
        else if ( strstr( scenePath, ".pbf" ) )
        {
            std::cout <<scenePath <<" is a pbf file\n";
            p_pbrtScene = pbrt::Scene::loadFrom( scenePath );
        }

        if ( p_pbrtScene )
        {
            std::cout <<"Was able to load the scene file\n";

            p_pbrtScene->makeSingleLevel();

            pbrt::Object::SP rootObject = p_pbrtScene->world;

        }
        else
        {
            std::cout <<"Couldn't load scene file; creating default scene\n";
            Scene_Init( pScene, &stackAlloc );
        }
    }

    // ISampler* p_sampler = new StratifiedSampler( 3, 3, true, 10 );
    ISampler* p_sampler = new PixelSampler( 4, 0 );



    // IIntegrator* p_integrator = new WhittedIntegrator( p_sampler, renderCam, &renderBuffer );
    // IIntegrator* p_integrator = new DirectLightingIntegrator(   nullptr /*sampler*/,
    //                                                             renderCam,
    //                                                             &renderBuffer,
    //                                                             LightStrategy::UniformSampleOne );

    // IIntegrator* p_integrator = new PathTracingIntegrator( p_sampler, renderCam, &renderBuffer, 8 /* max bounces */ );
    auto* p_integrator = new PathTracingIntegrator( p_sampler, renderCam, &renderBuffer, 16 /* max bounces */ );

    StackAlloc renderLoopArena( ALLOC_BYTES( stackAlloc, MB(100)), MB(100) );
    p_integrator->Init( pScene, &renderLoopArena );

    StackAlloc pixelDebuggerArena( ALLOC_BYTES( stackAlloc, MB(5)), MB(5) );

    std::cout <<"[FIRING DEBUG RAY]\n";
    vec2 pixelPos = 0.5f*cameraDim;
    // PixelDebugger( pScene, p_sampler, &renderCam, &pixelDebuggerArena, cameraDim, pixelPos );
    PixelDebugger( pScene, p_integrator->m_perThreadSampler[3], &renderCam, &pixelDebuggerArena, cameraDim, pixelPos );
    std::cout <<"\n\nDONE FIRING DEBUG RAY\n\n\n";
    // return 1;

    SDL_Event e;
    bool quit = false;
    size_t numProgressions = 0u;
    bool prevLmbDown = false;
    bool renderPaused = false;
    int mouseX = 0;
    int mouseY = 0;
    while ( !quit )
    {

        bool lmbDown = false;

        while ( SDL_PollEvent( &e ) )
        {
            if ( e.type == SDL_QUIT )
            {
                quit = true;
            }
            else if (e.type == SDL_KEYDOWN)
            {
                // quit = true;
                switch (e.key.keysym.sym)
                {
                    // case SDLK_LEFT:  x--; break;
                    // case SDLK_RIGHT: x++; break;
                    // case SDLK_UP:    y--; break;
                    // case SDLK_DOWN:  y++; break;
                    case SDLK_p: {
                        renderPaused = !renderPaused; 

                        SDL_SetWindowTitle( p_sdlWindow, renderPaused ?
                            "Harmonic PT - RENDER PAUSED" : "Harmonic PT" );
                    }break;
                }
            }
            else if (e.type == SDL_MOUSEBUTTONDOWN)
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                        // SDL_ShowSimpleMessageBox(0, "Mouse", "Left button was pressed!", p_sdlWindow);
                        lmbDown = true;
                        break;
                    case SDL_BUTTON_RIGHT:
                        // SDL_ShowSimpleMessageBox(0, "Mouse", "Right button was pressed!", p_sdlWindow);
                        break;
                    default:
                        // SDL_ShowSimpleMessageBox(0, "Mouse", "Some other button was pressed!", p_sdlWindow);
                        break;
                }
            }
            else if ( e.type == SDL_MOUSEMOTION )
            {
                mouseX = e.motion.x;
                mouseY = e.motion.y;
            }
        }

        const bool lmbReleased = prevLmbDown and !lmbDown;

        //  Run the pixel debugger
        //
        if ( lmbReleased )
        {
            pixelDebuggerArena.Reset();
            // BREAKPOINT(); // Trigger a breakpoint if a debugger is attached

            std::cout <<"mouse pos = (" << mouseX <<", " << mouseY <<")\n";
            vec2 pixelPos = { float( mouseX ), float( mouseY ) };
            PixelDebugger( pScene, p_sampler, &renderCam, &pixelDebuggerArena, cameraDim, pixelPos );
        }



        if ( !renderPaused and ( numProgressions < numSamples ) )
        {
            // printf("Progression %zu/%zu ...\r", numProgressions + 1u, numSamples );
            std::clog <<"Progression " <<numProgressions+1u <<"/" <<numSamples <<" ...\r";
            ++numProgressions;

            //  Perform one progression of the ray tracing loop
            // 
            p_integrator->Render( pScene, &renderLoopArena );

            //  Convert HDR image to LDR
            //  This LDR image buffer will be blitted to the screen buffer via SDL
            //
            //  TODO:: Allow user to control exposure
            //  TODO:: Actual tone mapping!
            //  
            {
                float invProgressions = 1.f/float(numProgressions);

                #pragma omp parallel for
                for ( size_t j = 0; j < imageHeight; ++j )
                {
                    for ( size_t i = 0; i < imageWidth; ++i )
                    {
                        size_t baseIndex = ( j * imageWidth + i ) * 3U;
                        int ir = pow( pImageBuffer[ baseIndex ]      * invProgressions, 2.2 ) * 255;
                        int ig = pow( pImageBuffer[ baseIndex + 1U ] * invProgressions, 2.2 ) * 255;
                        int ib = pow( pImageBuffer[ baseIndex + 2U ] * invProgressions, 2.2 ) * 255;

                        CLAMP_VAR( ir, 0, 255 );
                        CLAMP_VAR( ig, 0, 255 );
                        CLAMP_VAR( ib, 0, 255 );

                        pDisplayImageBuffer[ baseIndex ]        = byte( ir );
                        pDisplayImageBuffer[ baseIndex + 1 ]    = byte( ig );
                        pDisplayImageBuffer[ baseIndex + 2 ]    = byte( ib );
                    }
                }
            }

            SDL_BlitSurface( renderSurface, nullptr, gScreenSurface, nullptr );
            SDL_UpdateWindowSurface( p_sdlWindow );

            if ( numProgressions == numSamples )
            {
                printf("\nRENDER FINISHED!\n");

                SDL_SetWindowTitle( p_sdlWindow, "Harmonic PT - RENDER FINISHED" );
            }

        }


        prevLmbDown = lmbDown;
    }

    // Close and destroy the window
    //
    SDL_DestroyWindow( p_sdlWindow );

    // Clean up
    //
    SDL_Quit();

    //  Write image to file
    //
    {
        FILE* pRenderFile;
        const char* renderFilePath = cmdLineArgs.imagePath;
        pRenderFile = fopen( renderFilePath, "w" );
        // fopen_s( &pRenderFile, renderFilePath, "w" );
        if ( pRenderFile )
        {
            fprintf( pRenderFile, "P3 %d %d\n255\n", int(imageWidth), int(imageHeight) );

            for ( size_t j = 0; j < imageHeight; ++j )
            {
                for ( size_t i = 0; i < imageWidth; ++i )
                {
                    size_t baseIndex = ( j * imageWidth + i ) * 3U;

                    int ir = pDisplayImageBuffer[ baseIndex ];
                    int ig = pDisplayImageBuffer[ baseIndex + 1 ];
                    int ib = pDisplayImageBuffer[ baseIndex + 2 ];

                    fprintf( pRenderFile, "%d %d %d\n", ir, ig, ib );
                }
            }

            fclose( pRenderFile );  
            printf("Wrote out render file\n");
        }
        else
        {
            printf("Coudn't create '%s'\n", renderFilePath );
        }
    }

    return 0;
}
