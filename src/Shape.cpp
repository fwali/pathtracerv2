#include "Shape.h"
#include "Sampling.h"

IShape::~IShape() {}

float
IShape::Pdf( const Interaction& ref, const vec3& wi ) const
{
    Ray ray = ref.CreateRay( wi );
    IntersectionResult intRes {};

    if (!this->Intersects( ray, intRes ) )
    {
        return 0.f;
    }

    float pdf = lengthSquared( ref.m_pos - intRes.m_contactPos ) /
                ( absDot( intRes.m_contactNormal, -1.f*wi ) * this->Area() );

    return pdf;
}

void
Shape_Sphere::GetShadingInfo( SurfaceInteraction& o_surfInt, const IntersectionResult& intRes ) const
{
    //  
    //
    vec3 p = intRes.m_contactNormal;  // represent the eqvuilant intersection point on the unit sphere

    // defines the angle on the X-Z plane, going counter clockwise from +X axis; domain: [ 0, 2*pi )
    float phi = atan2( p.z, p.x );
    // defines the angle on the X-Y plane, going counter clockwise from the +X axis; domain: [ -pi/2, pi/2 ]
    float theta = asin( p.y );

    if ( phi < 0.f ) phi += TAU;

    vec2 uv = {
        fmod( m_uvScale.u*(phi / TAU), 1.0f ),
        fmod( m_uvScale.v*( theta + PI/2.f ) / PI, 1.f )
    };

    float sinTheta = p.y;
    float cosTheta = sqrtf( _MAX( 0.f, 1.f - sinTheta*sinTheta ) );
    float cosPhi = p.x / cosTheta;
    float sinPhi = p.z / cosTheta;

    vec3 dpdu = { -sinTheta*cosPhi, cosTheta, -sinTheta*sinPhi };
    vec3 dpdv = { -cosTheta*sinPhi, 0.f, cosTheta*cosPhi };

    vec3 d2pdu2 = { -cosTheta*cosPhi, -sinTheta, -cosTheta*sinPhi };
    vec3 d2pduv = { sinTheta*sinPhi, 0.f, -sinTheta*cosPhi };
    vec3 d2pdv2 = { -cosTheta*cosPhi, 0.f, -cosTheta*sinPhi };

    // First fundamental forms
    //
    float E = dot( dpdu, dpdu );
    float F = dot( dpdu, dpdv );
    float G = dot( dpdv, dpdv );

    vec3 norm = normalize( cross( dpdu, dpdv ) );

    float e = dot( norm, d2pdu2 );
    float f = dot( norm, d2pduv );
    float g = dot( norm, d2pdv2 );

    float denom = 1.f/( E*G - F*F );    // inverse determinant for second fundamental form
    vec3 dndu = (f*F - e*G)*denom*dpdu + (e*F - f*E)*denom*dpdv;
    vec3 dndv = (g*F - f*G)*denom*dpdu + (f*F - g*E)*denom*dpdv;

    //  Set shading geometry and partial derivatives on SurfaceInteraction object
    //
    o_surfInt = SurfaceInteraction( intRes.m_contactPos, uv, norm,
                                    dpdu, dpdv, dndu, dndv, 0.f /* F_TODO:: Put actual time */ );

    o_surfInt.m_posError = fwGamma( 5 ) * abs( intRes.m_contactPos );
}

Interaction
Shape_Sphere::Sample( const vec2& u ) const
{
    vec3 n = UniformSampleSphere( u );
    vec3 p = m_sphere.m_centre + m_sphere.m_radius*n;

    Interaction intx( p, n, 0.f /* F_TODO:: TIME */ );
    // F_TODO:: Take reversed shape orientation into account

    intx.m_posError = fwGamma(5) * abs( p );

    return intx;
}

Interaction
Shape_Sphere::Sample( const Interaction& ref, const vec2& u ) const
{
    vec3 centre         = m_sphere.m_centre;
    float radius        = m_sphere.m_radius;
    vec3 refP           = ref.m_pos;
    vec3 fromRefPoint   = centre - refP;

    //  Create a coordinate system on the sphere riented toward the ref point
    //
    vec3 _z             = normalize( fromRefPoint );
    vec3 _x              {};
    vec3 _y              {};
    CreateCoordSystem( _z, _x, _y );

    if ( lengthSquared( fromRefPoint ) < radius * radius )
    {
        return this->Sample( u );
    }

    float sinThetaMax2  = radius * radius / ( lengthSquared( fromRefPoint ) );
    float cosThetaMax   = std::sqrt( _MAX( 0.f, 1.f - sinThetaMax2 ) );
    float cosTheta      = (1.f - u.x) + u.x * cosThetaMax;
    float sinTheta      = std::sqrt( _MAX( 0.f, 1.f - cosTheta*cosTheta ) );
    float phi           = u.y * TAU;

    float distToCentre  = length( fromRefPoint );
    float distToSurface =   distToCentre*cosTheta -
                            std::sqrt( _MAX( 0.f, radius*radius - distToCentre*distToCentre * sinTheta*sinTheta ) );
    float cosAlpha      = ( distToCentre*distToCentre + radius*radius - distToSurface*distToSurface) /
                            ( 2.f * distToCentre * radius );
    float sinAlpha      = std::sqrt( _MAX( 0.f, 1.f - cosAlpha*cosAlpha ) );

    //  Use above basis vectors to determine world space normals s
    //  
    //  Note::  { _x, _y, _z } is a orthonormal basis centred around the ref point
    //          { -_x, -_y, -_z } is an equivalent basis centred around a point lying on the sphere
    //  
    vec3 sphereNormal   =   cosTheta * sin( phi ) * ( -1.f*_x ) +
                            sinTheta * sin( phi ) * ( -1.f*_y ) +
                            cos( phi ) * ( -1.f*_z );
    
    vec3 sphereP            = radius * sphereNormal;
    sphereP *= radius / length( sphereP );
    vec3 pError = fwGamma( 5 ) * abs( sphereP );

    Interaction intx( sphereP + centre + 10*EPSILON*sphereNormal, sphereNormal, 0.f /* F_TODO:: TIME */ );
    intx.m_posError = pError;

    return intx;
}

float
Shape_Sphere::Pdf( const Interaction& ref, const vec3& wi ) const
{
    // return IShape::Pdf( ref, wi );
    float pdf = 0.f;

    vec3 centre = m_sphere.m_centre;
    float radius = m_sphere.m_radius;
    vec3 origin = OffsetRayOrigin( ref.m_pos, ref.m_posError, ref.m_normal, centre - ref.m_pos );

    if ( lengthSquared( origin - centre ) <= radius*radius )
    {
        return IShape::Pdf( ref, wi );
    }

    float sinThetaMax2 = radius*radius / lengthSquared( ref.m_pos - centre );
    float cosThetaMax  = std::sqrt( _MAX( 0.f, 1.f - sinThetaMax2 ) );
    return UniformConePdf( cosThetaMax );
}

bool Intersection_Ray_Triangles_Indexed(
	IntersectionResult& result,
	const Ray*			pRay,
    const u32*          pIndices,
	const vec3*			pVerts)
{
	// Intersection info
	size_t  index 	= ~((u64)0);
    float   tMin    = _MIN( result.m_tMin, pRay->m_tMax );

	// Triangle verts
	vec3 V0 = {};
	vec3 V1 = {};
	vec3 V2 = {};

	// Edges
	vec3 E1 = {};
	vec3 E2 = {};

	vec3 P  = {};
	vec3 Q  = {};
	vec3 T  = {};
	float det = 0.0f;
	float invDet = 0.0f;
	
	// Barycentric co-efficients
	float u = 0.0f;
	float v = 0.0f;

	// cached ray info
	vec3 rayDir = pRay->m_dir;
	vec3 rayOrigin = pRay->m_origin;
	vec3 triNormal = {};

	// Find closest intersection with ray in list of triangles
    bool intersection = false;
	for (size_t i = 0; i < 1u; ++i)
	{
        V0 = pVerts[ pIndices[ 0 ] ];
        V1 = pVerts[ pIndices[ 1 ] ];
        V2 = pVerts[ pIndices[ 2 ] ];

		E1 = V1 - V0;
		E2 = V2 - V0;

		P = cross( rayDir, E2 );
		det = dot( E1, P );

		if (det > -EPSILON && det < EPSILON) { continue; }
		invDet = 1.0f/det;

		T = rayOrigin - V0;
		u = dot( T, P ) * invDet;
		if (u < 0.0f || u > 1.0f) { continue; }

		Q = cross( T, E1 );
		v = dot(rayDir, Q)*invDet;
		if (v < 0.0f || u + v > 1.0f) { continue; }

		float t = dot(E2, Q)*invDet;

		if (t > 0.0f && t < tMin)
		{
			tMin = t;
			index = i;
            intersection = true;
		}
	}

	if ( intersection )
	{
        result.m_hit                = true;
		result.m_index              = index;
		result.m_contactPos         = Ray_AtT(*pRay, tMin);
		result.m_contactNormal      = normalize( cross( E1, E2 ) );
		result.m_tMin               = tMin;

        float w = 1.f - u - v;  // final barycentric coordinate
        result.m_baryCoord = { u, v, w };
	}

	return intersection;
}


AABB
TriangleShape::ComputeBBox() const
{
    AABB bbox;

    AABB_Expand( bbox, m_triMeshPtr->m_pVerts[ m_indexPtr[0] ] );
    AABB_Expand( bbox, m_triMeshPtr->m_pVerts[ m_indexPtr[1] ] );
    AABB_Expand( bbox, m_triMeshPtr->m_pVerts[ m_indexPtr[2] ] );

    return bbox;
}

float
TriangleShape::Area() const
{
    vec3 P[3] = {
        m_triMeshPtr->m_pVerts[ m_indexPtr[ 0 ] ],
        m_triMeshPtr->m_pVerts[ m_indexPtr[ 1 ] ],
        m_triMeshPtr->m_pVerts[ m_indexPtr[ 2 ] ]
    };

    return 0.5f*length( cross(P[1] - P[0], P[2] - P[0]) );
}

void 
TriangleShape::GetShadingInfo( SurfaceInteraction& o_surfInt, const IntersectionResult&  intRes ) const
{
    u32 i0 = m_indexPtr[ 0 ];
    u32 i1 = m_indexPtr[ 1 ];
    u32 i2 = m_indexPtr[ 2 ];

    //  ( bc.x, bc.y, bc.z ) correspond to the barycentric weights ( u, v, w )
    //
    vec3 bc = intRes.m_baryCoord;

    //  Use the geometric normal, and if the mesh has defined normals, use barycentric interpolation
    //
    vec3 normal = intRes.m_contactNormal;
    if ( m_triMeshPtr->m_pNormals )
    {
        normal = normalize( 
                    bc.x * m_triMeshPtr->m_pNormals[ i0 ] +
                    bc.y * m_triMeshPtr->m_pNormals[ i1 ] +
                    bc.z * m_triMeshPtr->m_pNormals[ i2 ] );
    }


    vec3 P[3] = {
        m_triMeshPtr->m_pVerts[ i0 ],
        m_triMeshPtr->m_pVerts[ i1 ],
        m_triMeshPtr->m_pVerts[ i2 ],
    };

    vec2 UV[3] = {
        { 0.0f, 0.0f },
        { 1.0f, 0.0f },
        { 1.0f, 1.0f }
    };

    if ( m_triMeshPtr->m_pUVs )
    {
        UV[ 0 ] = m_triMeshPtr->m_pUVs[ i0 ];
        UV[ 1 ] = m_triMeshPtr->m_pUVs[ i1 ];
        UV[ 2 ] = m_triMeshPtr->m_pUVs[ i2 ];
    }

    //  Compute barycentric interpolation of UV coordinates
    //  
    vec2 uv = bc.x*UV[0] + bc.y*UV[1] + bc.z*UV[2];

    vec3 dpdu {};
    vec3 dpdv {};

    //  Compute dpdu, dpdv
    //
    vec2 duv02 = UV[ 0 ] - UV[ 2 ];
    vec2 duv12 = UV[ 1 ] - UV[ 2 ];
    vec3 dp02  = P[ 0 ] - P[ 2 ];
    vec3 dp12  = P[ 1 ] - P[ 2 ];
    float determinant = duv02.d[0] * duv12.d[1] - duv02.d[1]*duv12.d[0];
    if ( determinant == 0.f )
    {
        CreateCoordSystem( normal, dpdu, dpdv );
    }
    else
    {
        float invDet = 1.f/determinant;
        dpdu = invDet*( duv12.d[1]*dp02 - duv02.d[1]*dp12 );
        dpdv = invDet*( -duv12.d[0]*dp02 + duv02.d[0]*dp12 );
    }

    // F_TODO:: Compute dndu and dndv for shading geometry\

    //  Set shading geometry and partial derivatives on SurfaceInteraction object
    //
    o_surfInt = SurfaceInteraction( intRes.m_contactPos, uv, normal,
                                    dpdu, dpdv, {} /*dndu*/, {} /*dndv*/, 
                                    0.f /* F_TODO:: Do time properly */ );

}

bool
TriangleShape::Intersects( const Ray& ray, IntersectionResult& intRes ) const
{
    return Intersection_Ray_Triangles_Indexed(
            intRes,
            &ray,
            m_indexPtr,
            m_triMeshPtr->m_pVerts );
}

Interaction
TriangleShape::Sample( const vec2& u ) const
{
    return {};
}