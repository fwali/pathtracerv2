#!/bin/bash

BIN_NAME=PathTracer
OUTPUT_DIR=./bin/debug

SRC_FILES="src/main.cpp"

LIBS="  -lSDL2 \
        -lpbrtParser_static"

DISABLE_WARNINGS="  -Wno-c++98-compat-pedantic \
                    -Wno-old-style-cast \
                    -Wno-gnu-anonymous-struct \
                    -Wno-nested-anon-types \
                    -Wno-unknown-pragmas \
                    -Wno-missing-braces \
                    -Wno-padded \
                    -Wno-unused-parameter"

LLVM_CLANG="/usr/local/opt/llvm/bin/clang++"
LPATHS="-L/usr/local/opt/llvm/lib/ -L/usr/local/opt/sdl2/lib/ \
         -L./libs"
IPATHS="-I/usr/local/opt/llvm/include/c++/v1/ \
		-I/usr/local/include/ \
        -I./src/"

# Necessary to find the right includes path in macOS Catalina
#
export SDKROOT="$(xcrun --sdk macosx --show-sdk-path)"

BUILD_CMD="$LLVM_CLANG $LPATHS $IPATHS -std=c++11 -fopenmp $DISABLE_WARNINGS -W -O3 -m64 $LIBS $SRC_FILES -o $OUTPUT_DIR/$BIN_NAME"
# BUILD_CMD="$LLVM_CLANG $LPATHS $IPATHS -std=c++11 -fopenmp $DISABLE_WARNINGS -W -m64 $LIBS $SRC_FILES -o ..$OUTPUT_DIR/$BIN_NAME"

# Make build output directory
mkdir -p $OUTPUT_DIR >& /dev/null
mkdir -p build_osx >& /dev/null

echo Building $BIN_NAME

$BUILD_CMD

# Clean up temp object files
mkdir -p build/
# mv *.o build/

# cd build_osx
# $BUILD_CMD
# cd ../

#osascript -e 'display notification "Build Finished!!" with title "PathTracer"'
